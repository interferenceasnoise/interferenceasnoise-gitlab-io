---
layout: page
title: Education
categories: edu
permalink: edu/
published: true
hide_in_nav: true
---

In addition to our research contributions, one of the primary missions of the Wireless Lab at UCLA is to educate and train wireless engineers. 
We are invested in equipping students with the technical background and skills to help them be successful in the classroom, in research, and in the workplace.
We accomplish this in multiple ways:
- by teaching courses at UCLA
- by hosting short courses and workshops
- through outreach events
- through collaboration.

### Current and Upcoming Course Offerings

Prof. Ian Roberts either recently taught, is currently teaching, or is expected to teach the following electrical engineering courses at UCLA.
- **2024 Winter:** ECE 239AS Advanced Techniques and Technologies in Modern Wireless Communications. This special topics course highlights the underlying physical layer (PHY) techniques and technologies that are core to modern wireless networks like 5G and Wi-Fi.
- **2024 Spring:** ECE 132A Introduction to Communication Systems. This course combines foundational concepts in linear systems/signals, digital signal processing, and probability to establish the elegant techniques by which we communicate information and analyze the performance of such techniques.

This list is subject to change. Please see the official list of course offerings for the most accurate information.

### Communications Course Sequence at UCLA

#### Undergraduate

If you are an **undergraduate** student at UCLA interested in communications, the following sequence of courses have direct applications to modeling, analyzing, designing, and optimizing communication systems:
- ECE 102 -- Systems and Signals. This course provides the necessary foundations for understanding how to mathematically model communication signals and elements which affect them.
- ECE 113 -- Digital Signal Processing. This course teaches the essential tools and fundamentals needed to mathematically process and analyze communication signals.
- ECE 113D + 113DW -- Digital Signal Processing Design. These two courses provide the opportunity to implement concepts and tools learned in ECE 113 on actual hardware platforms.
- ECE 131A -- Probability and Statistics. This course introduces notions of randomness and statistical characterization, which are necessary to quantify uncertainty/imperfections and analyze communication system performance.
- ECE 132A -- Introduction to Communication Systems. This course combines concepts from all the foundational courses listed above to establish the elegant techniques by which we communicate information and analyze communication system performance.
- ECE 132B -- Data Communications and Telecommunication Networks. This course overviews architectures, flow control, medium access, error control, and queueing modeling/analysis of communication networks.
- ECE 133A -- Applied Numerical Computing. This course covers fundamentals of linear algebra from both an analytical and numerical perspective.


#### Graduate 

If you are a **graduate** student at UCLA interested in communications, the following courses would be of interest, not necessarily taken in the order listed:
- ECE 230A -- Detection and Estimation in Communication. 
- ECE 230B -- Digital Communication Systems. 
- ECE 231A -- Information Theory.
- ECE 231E -- Channel Coding Theory. 
- ECE 233 -- Wireless Communication Systems.
- ECE 236A -- Linear Programming.
- ECE 236B -- Convex Optimization.
- ECE 239AS -- Special Topics in Signals and Systems.
- ECE 241A -- Stochastic Processes.
- ECE C247 -- Neural Networks and Deep Learning.
- ECE 262 -- Antenna Theory and Design.

A full list of UCLA's ECE course offerings can be found at [this link](https://registrar.ucla.edu/academics/course-descriptions?search=EC+ENGR).



### Past Courses at UCLA

Past courses taught by Prof. Ian Roberts: 
- **2024 Winter:** ECE 239AS Advanced Techniques and Technologies in Modern Wireless Communications. This special topics course highlights the underlying physical layer (PHY) techniques and technologies that are core to modern wireless networks like 5G and Wi-Fi.



