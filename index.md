---
layout: home
bio-image_: /images/people/ipr-headshot-crop-4.jpg
bio-image: /images/ucla/39.jpg
bio-image-mobile: /images/artwork_lab_03_svg.svg
bio-image-mobile: /images/ucla/03.jpg
bio-tag: >
  Welcome to the Wireless Lab at UCLA, directed by <a href="https://ianproberts.com" target="_blank">Prof. Ian Roberts</a>.
bio-summary: >
  We conduct theoretical and experimental research on wireless communication and sensing at <a href="https://www.ee.ucla.edu" target="_blank">UCLA</a>.
bio-desc: >
  We use mathematical tools, machine learning, and real-world hardware to analyze, model, optimize, and prototype wireless systems and solutions.
---



<!---
{% assign f = "artwork_lab_03_svg.svg" %}
{% assign c = '' %}
{% assign w = 100 %}
{% include image.html file=f caption=c link=true width=w %}
--->


{% include alert-begin.html title="Prospective Students" %}
<div markdown="1">
We are currently recruiting students to join the {{ site.lab-name }} at UCLA in conducting research that aims to advance wireless communication systems like 5G/6G and Wi-Fi.
Our projects span from theory to prototyping, so there is likely a good fit for you on one of them.
We cater to the strengths and interests of each of our students when considering their research topics.

If you are a **prospective graduate student** interested in joining our lab, please [apply](https://www.ee.ucla.edu/graduate-application-requirements/) to the electrical and computer engineering program at UCLA. The application deadline is December 15. Include my name (Ian Roberts) in your application. 

If you are **currently an undergraduate or master's student at UCLA** interested in doing research during the academic year or over the summer, please email me.
We are happy to host undergraduate and master's researchers in our lab.

For those not at UCLA, we are also happy to host **visiting students/researchers**.

Unfortunately, I cannot always promptly respond to every email I receive, but I will do my best.
</div>
{% include alert-end.html %}

<!---
{% include alert-begin.html title="Prospective Students" %}
<div markdown="1">
We are currently recruiting students to join the {{ site.lab-name }} at UCLA in conducting research that aims to transform wireless communication systems.

We are interested in students from anywhere and with any background in wireless.
Our projects span from theory to prototyping, so there is likely a good fit for you on one of them.
We cater to the strengths and interests of each of our students when considering their research topics.

We are invested in developing our students into capable, independent researchers that can succeed in industry, academia, and government labs.
Our goal is for each student to have an enjoyable, productive Ph.D. that they can be proud of.

If you are a prospective graduate student interested in joining our lab, please do the following:
- Apply to the electrical and computer engineering program at UCLA. Include my name (Ian Roberts) in your application. 
- Send me a brief email at `ianroberts [at] ucla [dot] edu` with your CV attached as a PDF. If you are debating between M.S. or Ph.D., please include that in your email.

If you are currently an undergraduate or master's student at UCLA interested in doing research during the academic year or over the summer, please email me.
We are happy to host undergraduate and master's researchers in our lab.
For those not at UCLA, we are also happy to host visiting students/researchers.

Unfortunately, I cannot necessarily respond to every email I receive, but I will do my best.
</div>
{% include alert-end.html %}
--->

<!---
## Recent News

{% assign n = (site.data.news | sort: 'date') | reverse %}
{% for news in n limit:10 %}
- {{ news.date | date: "%B %Y" }}: {{ news.text }}  {% endfor %}
--->


## About the {{ site.lab-name }}

In the {{ site.lab-name }} at UCLA, we use mathematical tools, machine learning, and real-world hardware to analyze, model, optimize, and prototype wireless systems and solutions.
We are particularly interested in conducting research and creating innovative technologies to drive the design, development, and deployment of future 6G cellular networks.
<!---We are particularly interested in creating innovative technologies that will be used in future 6G cellular systems.--->

{% assign f = "/fig/evolution_13.svg" %}
{% assign c = "New spectrum, topologies, and tools will deliver the connectivity required by emerging applications." %}
{% include image.html file=f caption=c link=false width=80 %}

We conduct research on a variety of topics in wireless, all of which have relevant applications in next-generation wireless communication systems.
Broadly speaking, our work aims to strengthen the existing foundation of 5G networks while also introducing transformative technologies for 6G networks.
In this pursuit, our current projects are on the following research topics, all of which we expect to have real-world impact on future wireless connectivity:
- **Full-Duplex Wireless Systems** --- to increase throughput, reduce latency, and broaden coverage; 
- **Satellite Communication Systems** --- to deliver near-global wireless connectivity;
- **Intelligent Beam Alignment** --- to efficiently deploy high-frequency communication systems;
- **Joint Communication and Sensing** --- to enable emerging and unforeseen applications.


<!---
- full-duplex wireless systems --- to increase throughput, reduce latency, and broaden coverage; 
- LEO satellite communication systems --- to deliver near-global wireless connectivity;
- intelligent beam alignment --- to efficiently deploy high-frequency communication systems in the real-world;
- joint communication and sensing --- to enable emerging and unforeseen applications.
-->

<!---it, our current projects are on the following research topics, all of which we expect to have real-world impact on future 6G networks and wireless connectivity in the coming years:
- full-duplex wireless communication systems;
- low-Earth orbit satellite communication systems;
- beam alignment in mmWave and sub-THz systems;
- joint communication and sensing.
--->

{% assign f = "fig/evolution_16.svg" %}
{% assign c = 'Our research aims to strengthen the 5G foundation and create innovative technologies for 6G.' %}
{% assign w = 85 %}
{% include image.html file=f caption=c link=false width=w %}

We are excited and optimistic about the future of wireless connectivity and the roles it will have in society.
We pride ourselves on conducting meaningful research that can have real-world impact. 
Here are some of our papers we recommend you read if you're interested in our work:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf)
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf)
- [Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range](/pdf/pub/bflrdr.pdf)
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf)
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf)
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf)
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf)
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf)

<!---
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf) <span class="badge badge-grey-inverted">Overview</span> 
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf) <span class="badge badge-blue-inverted">Magazine</span>
- [Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range](/pdf/pub/bflrdr.pdf) <span class="badge badge-purple-inverted">Theory</span>
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
--->

<!---
Here are some of our papers we recommend you read if you're interested in our work:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf) <span class="badge">Overview</span>
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf) <span class="badge">Magazine</span>
- [Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range](/pdf/pub/bflrdr.pdf) <span class="badge">Theory</span>
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf) <span class="badge">Theory</span>
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf) <span class="badge">Experimental</span>
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf) <span class="badge">Experimental</span>
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf) <span class="badge">Experimental</span>
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf) <span class="badge">Experimental</span>
--->

<a class="callout-button" href="/research">Our Research &#8674;</a>

<!---
Please see our [Research page](/research) for more on what we do, and see our [Publications page](/publications/) for a full list of publications.


{% assign f = "/fig/evolution_10.svg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=false width=80 %}
--->


## Our Lab's Mission

We have many motivations for what we do in the Wireless Lab at UCLA. 
At the core of everything we do is our mission to continually accomplish three overarching goals:
- to conduct meaningful research that advances wireless connectivity;
- to educate and train wireless engineers capable of solving real-world problems;
- to excite others about the underlying technologies connecting our world.

We strive to accomplish this mission through the combination of research, education, mentorship, and outreach. 


## Three Recent Publications

{% assign pubs = (site.data.publications | sort: 'date' | reverse) %}
{% for p in pubs limit:3 %}
{% include publication.html %}  
{% endfor %}

<a class="callout-button" href="/publications">All Publications &#8674;</a>
&nbsp;&nbsp;<a href="https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works" target="_blank" class="callout-button">Google Scholar &#8599;</a>


<!---
## About Prof. Ian Roberts

{% include text-image-begin.html image="people/ipr.jpg" width=25 mobile=1 %}
<div markdown="1">
[Ian P. Roberts](https://ianproberts.com) is an Assistant Professor in the [Department of Electrical and Computer Engineering](https://ee.ucla.edu) at [UCLA](https://ucla.edu), where he leads the Wireless Lab in conducting theoretical and experimental research on wireless communication and sensing. 
Prior to joining UCLA, he completed his Ph.D. at the [University of Texas at Austin](https://www.ece.utexas.edu), where he was an NSF Graduate Research Fellow in the [Wireless Networking and Communications Group](https://wncg.org) and the [6G@UT Research Center](https://6g-ut.org/).

He has industry experience developing and prototyping wireless technologies at AT&T Labs, Amazon, GenXComm (startup), Sandia National Labs, and Dynetics (defense contractor). 
During his time at [GenXComm](https://www.gxc.io/), he created and demonstrated their earliest wireless solutions combining RF, photonic, and digital signal processing for interference cancellation. 

In 2023, he received the [Andrea Goldsmith Young Scholars Award](https://comt.committees.comsoc.org/awards/andrea-goldsmith-young-scholars-award/) from the [Communication Theory Technical Committee](https://comt.committees.comsoc.org) of the [IEEE Communications Society](https://www.comsoc.org/) for his work on the theory and practice of full-duplex millimeter-wave communication systems. He is a licensed amateur radio operator; his callsign is [KE0QVW](https://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=4013155).
</div>
{% include text-image-end.html %}
--->

## About UCLA

{% include text-image-begin.html image="ucla/07.jpg" width=45 mobile=1 float='right' %}
<div markdown="1">
UCLA is located on a large, beautiful campus in the Westwood neighborhood of Los Angeles, California. 
It is consistently ranked among the top universities in the world, both holistically and across many indvidual fields including engineering. 
UCLA is particularly unique in that it is exceptionally well-rounded, offering first-class opportunities, education, and training for people of all intrests and backgrounds.

The electrical and computer engineering (ECE) department at UCLA includes some of the most accomplished researchers in their respective disciplines. 
Courses offered by UCLA ECE faculty are designed to equip students with strong analytical foundations and relevant practical skills that are necessary to succeed in industry, academia, and government labs. 

In particular, UCLA is regarded as one of the top schools in the world for students interested in wireless communication and sensing.
For such students, UCLA ECE offers a broad range of courses covering every aspect of wireless systems: from theory to implementation.
We offer courses on signal processing for communications, wireless communication theory, multi-antenna communications, RF circuit design, antenna theory, information theory, communication networks, optimization, and machine learning, along with several others.
Following graduation, UCLA ECE students go on to develop cutting-edge technologies at companies such as Qualcomm, Broadcom, Viasat, Samsung, Google, Apple, Intel, Cisco, NVIDIA, Ericsson, Nokia, IBM, and countless others.

<a class="callout-button" href="/ucla/">More Photos of UCLA &#8674;</a>
<!---<a href="/ucla/" class="badge badge-blue-inverted">More Photos of UCLA</a>-->
</div>
{% include text-image-end.html %}

<!--
## Our Research in the News

{% assign press = (site.data.press | sort: 'date' | reverse) %}
{% for p in press limit:100 %}
- {{ p.date | date: "%B %Y" }}: [{{ p.title }} ({{ p.publisher }})]({{ p.link }})  {% endfor %}
-->




## Contact

Please reach out if you are interested in collaborating with us or would like additional information on our work. 

Email: ianroberts [at] ucla [dot] edu

Lab: 54-147B Engr IV

Ian's Office: 56-147A Engr IV

<!---
## Our Collaborators

{% assign ppl = (site.data.collaborators | sort: 'name') %}
{% for p in ppl %}
- [{{ p.name }}]({{ p.site }}), _{{ p.affil }}_  {% endfor %}
--->

[WNCG]: https://wncg.org
