---
layout: page
title: Publications
categories: publications
permalink: publications/
published: false
---


<a class="callout-button" href="/research">Our Research &#8674;</a>
&nbsp;&nbsp;<a href="https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works" target="_blank" class="callout-button">Google Scholar &#8599;</a>

<!---<a href="/research" class="badge badge-blue-inverted">Research</a>&nbsp;&nbsp;<a href="https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works" target="_blank" class="badge badge-blue-inverted">Google Scholar</a>--->

<div style="margin-top: 30px;">
<ul class="tree">
<li><span></span>Animals
 <ul>
  <li><span></span>Birds</li>
  <li><span></span>Mammals
   <ul>
    <li><span></span>Elephant</li>
    <li><span></span>Mouse</li>
   </ul>
  </li>
  <li><span></span>Reptiles</li>
 </ul>
</li>
<li><span></span>Plants
 <ul>
  <li><span></span>Flowers
   <ul>
    <li><span></span>Rose</li>
    <li><span></span>Tulip</li>
   </ul>
  </li>
  <li><span></span>Trees</li>
 </ul>
</li>
   </ul>
</div>

All publications listed on this page are authored by one or more members of the Wireless Lab.
Each publication contains theoretical formulations, optimization, and/or mathematical models, but several publications listed below are more experimental in nature than others, often involving actual hardware and real-world measurements.
To distinguish these publications having significant experimental components, next to its title will be the badge <span class="badge badge-magenta-inverted">Experimental</span>. Those without such a badge can therefore be assumed to be more theoretical in nature, often validated purely using simulation as appropriate.

<!--[Research](/research) \| [Google Scholar](https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works&sortby=pubdate)-->

{% assign pubs = (site.data.publications | sort: 'date') | reverse %}


### Preprint

{% for p in pubs %}
{% if p.type == 'preprint' %}
{% include publication.html %}  
<div>&nbsp;</div>
{% endif %}
{% endfor %}


### Journal and Magazine

{% for p in pubs %}
{% if p.type == 'journal' %}
{% include publication.html %}  
<div>&nbsp;</div>
{% endif %}
{% endfor %}


### Book Chapter

{% for p in pubs %}
{% if p.type == 'book' %}
{% include publication.html %}  
<div>&nbsp;</div>
{% endif %}
{% endfor %}


### Conference

{% for p in pubs %}
{% if p.type == 'conference' %}
{% include publication.html %}
<div>&nbsp;</div>
{% endif %}
{% endfor %}


### Disclaimer

Personal use of this material is permitted. Permission from respective copyright holders (e.g., IEEE, Springer) must be obtained for all other uses, in any current or future media, including reprinting/republishing this material for advertising or promotional purposes, creating new collective works, for resale or redistribution to servers or lists, or reuse of any copyrighted component of this work in other works.

Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or any other sponsors.

  
