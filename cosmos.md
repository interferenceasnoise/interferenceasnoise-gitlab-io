---
layout: page
title: COSMOS 2025
categories: cosmos
permalink: cosmos/
published: true
hide_in_nav: true
---


### Bit by Bit: Mathematics and Technologies of the Information Age

How does NASA communicate with the Voyager space probe over 15 billion miles from Earth? 
How does this relate to QR codes and .zip files? 
How do Wi-Fi and 5G deliver real-time video using wireless signals? 
In this cluster, we will explore how information is stored, protected, and reliably communicated in today’s increasingly digital world. 
Whether you are sending a text, streaming a video, or making an online purchase, information is constantly being transported from one point to another. 
How do we make sure that this information does not get lost, corrupted, or intercepted along the way? 
You will learn how special techniques are used to protect privacy, compress data, and detect and even fix errors when storing and communicating information. 
Then, you will learn how a transmitter encodes this data onto a wireless signal and how a receiver can reliably extract this data from that signal. 
This cluster will provide you with a deeper understanding of the mathematics and technologies underpinning the ongoing Information Age through hands-on learning, field trips around Los Angeles, the implementation of an end-to-end wireless communication system using software-defined radios.

{% assign f = "/fig/cosmos-e2e.png" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=false width=80 %}

### Instructors

#### Prof. Lara Dolecek

{% capture f %}people/dolecek.jpg{% endcapture %}
{% include text-image-begin.html image=f width=25 mobile=1 link=0 float='left' %}
<div markdown="1">
<span markdown="1">Prof. Lara Dolecek received her Ph.D. in Electrical Engineering and
Computer Science from UC Berkeley. She is currently a Full Professor in the
Department of Electrical and Computer Engineering and the Department of
Mathematics at UCLA. At UCLA, she founded and is the director of the
Laboratory for Robust Information Systems. Prof. Dolecek received numerous
awards for her research and teaching achievements, including NSF CAREER
Award, Northrop Grumman Excellence in Teaching Award, and the Intel
Faculty Award, among others. Prof. Dolecek frequently volunteers in K-12
STEM education activities. She is a co-founder of Los Angeles Computing Circle (LACC), an
outreach program at UCLA, currently in its 12th year, aimed at talented high schoolers who are
interested in careers in computing.</span>

</div>
{% include text-image-end.html %}



#### Prof. Ian Roberts

{% capture f %}people/ipr.jpg{% endcapture %}
{% include text-image-begin.html image=f width=25 mobile=1 link=0 float='left' %}
<div markdown="1">
<span markdown="1">Prof. Ian Roberts received his Ph.D. in Electrical and Computer Engineering
from the University of Texas at Austin, where he was an NSF Graduate
Research Fellow. He is currently an Assistant Professor in the Department of
Electrical and Computer Engineering at UCLA, where he is also the founding
director of the Wireless Lab at UCLA. At UCLA, Prof. Roberts currently serves
as the faculty advisor for the department’s Graduate Student and Postdoc
Society (GAPS) and the IEEE Student Branch’s Wireless, RF, and Analog
Project (WRAP). In 2023, he received the Andrea Goldsmith Young Scholars
Award from the Communication Theory Technical Committee of the IEEE Communications
Society for his research contributions in wireless communications.</span>

</div>
{% include text-image-end.html %}


### Prerequisites

The prerequisite for this class is Algebra 2. 
Students should also be somewhat familiar with vectors, matrices, and basic probability. 
Basic exposure to any programming language is preferred. 
However, in an effort to make this cluster inclusive, students will be introduced to any relevant Python programming that is necessary throughout the course via dedicated modules.


### Structure

This 4-week course will be divided into two 2-week sections. 
The first two weeks will focus on information coding techniques. 
The second two weeks will focus on communication of information using wireless signals. 
Lectures will be given each weekday morning for 2.5 hours by UCLA faculty, followed by a break for lunch. 
Afterwards, 4 hours of lab work with the teaching assistant will be used to reinforce lecture material through hands-on exercises, programming assignments, and course project modules. Through this lab work, students will have the opportunity to work individually and in groups. 
The course will culminate with a final project where students will employ the skills and knowledge attained throughout the entire cluster to implement a reliable end-to-end wireless communication system using software-defined radios.


### Tentative Schedule
 
- Week 1: Information Representation and Data Compression
- Week 2: Data Encryption and Error Correction
- Week 3: Fundamentals of Signals and Systems
- Week 4: Signal Processing for Communications


### About the COSMOS Program

COSMOS is an intensive four-week summer residential pre-college program for students who have demonstrated an aptitude for academic and professional careers in science, technology, engineering, and mathematics (STEM) subjects. 
Talented and motivated students completing grades 8--12 have the opportunity to be mentored by renowned faculty, researchers, and scientists in state-of-the-art facilities while exploring advanced STEM topics far beyond the courses usually offered in California high schools. 
Through challenging hands-on and lab-intensive curricula, COSMOS fosters its students' interests, skills, and awareness of educational and career options in STEM fields.

The mission of COSMOS is to motivate the most creative minds of the new generation of prospective scientists, engineers, and mathematicians who will become leaders for California, the nation, and the world. The program aims to create a community of students who participate in, and contribute to, an intensive academic experience that goes beyond the traditional high-school curriculum, created and delivered by distinguished STEM researchers and scholars.

Core partners of COSMOS include the legislative educational, corporate, and philanthropic sectors of California, including the State of California; faculty, staff, and students of the University of California, California State University, and California Community Colleges systems; STEM teachers from California high schools; and corporate and private funders and foundations who provide essential support to COSMOS. Talented high school students from the state's 58 counties, parents, and counselors complete the teaching and learning community that COSMOS brings together. 
Six UC campuses now serve as hosts: Davis, Irvine, Los Angeles, San Diego, and Santa Cruz.


[COSMOS Program Homepage](https://cosmos-ucop.ucdavis.edu/app/main/page/campuses-and-clusters)

[UCLA's COSMOS Homepage](https://cosmos.ucla.edu/)
