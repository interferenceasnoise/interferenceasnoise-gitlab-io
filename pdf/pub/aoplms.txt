@misc{ipr_aoplms,
    author={Hyowon Lee, Jungyeon Kim, Geon Choi, Ian P. Roberts, Jinseok Choi, Namyoon Lee},
    title={Nonlinear Self-Interference Cancellation with Learnable Orthonormal Polynomials for Full-Duplex Wireless Systems},
    year={2024},
    month=mar,
    url={https://arxiv.org/abs/2403.11094},
}

@misc{ipr_aoplms,
    author={Hyowon Lee, Jungyeon Kim, Geon Choi, Ian P. Roberts, Jinseok Choi, Namyoon Lee},
    title={Nonlinear Self-Interference Cancellation with Learnable Orthonormal Polynomials for Full-Duplex Wireless Systems},
    year={2024},
    month={Mar.},
    url={https://arxiv.org/abs/2403.11094},
}

