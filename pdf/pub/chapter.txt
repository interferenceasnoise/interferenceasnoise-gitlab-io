@incollection{ipr_fd_chapter,
    author      = {Ian P. Roberts and Himal A. Suraweera},
    title       = {Full-Duplex Transceivers for Next-Generation Wireless Communication Systems},
    editor      = {Yuanwei Liu and Jun Zhang and Xingqin Lin and Joongheon Kim},
    booktitle   = {Fundamentals of 6G Communications and Networking},
    publisher   = {Springer Nature},
    address     = {Switzerland},
    year        = 2024,
    pages       = {425--461},
    chapter     = {},
}

