@misc{ipr_abfparpc,
    author={A. Liu, I. P. Roberts, T. Riihonen, and W. Sheng},
    title={Analog Beamforming for In-Band Full-Duplex Phased Arrays with Quantized Phase Shifters under a Per-Antenna Received Power Constraint},
    year={2024},
    month=jan,
    url={https://arxiv.org/abs/2401.13914},
}

@misc{ipr_abfparpc,
    author={A. Liu, I. P. Roberts, T. Riihonen, and W. Sheng},
    title={Analog Beamforming for In-Band Full-Duplex Phased Arrays with Quantized Phase Shifters under a Per-Antenna Received Power Constraint},
    year={2024},
    month={Jan.},
    url={https://arxiv.org/abs/2401.13914},
}

