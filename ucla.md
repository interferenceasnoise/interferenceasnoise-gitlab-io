---
layout: page
title: About UCLA
categories: ucla
permalink: ucla/
public: true
published: true
hide_in_nav: true
---

{% assign w = 95 %}

UCLA is located on a large, beautiful campus in the Westwood neighborhood of Los Angeles, California. 
It is consistently ranked among the top universities in the world, both holistically and across many indvidual fields including engineering. 
UCLA is particularly unique in that it is exceptionally well-rounded, offering first-class opportunities, education, and training for people of all intrests and backgrounds.

The electrical and computer engineering (ECE) department at UCLA includes some of the most accomplished researchers in their respective disciplines. 
Courses offered by UCLA ECE faculty are designed to equip students with strong analytical foundations and relevant practical skills that are necessary to succeed in industry, academia, and government labs. 

In particular, UCLA is regarded as one of the top schools in the world for students interested in wireless communication and sensing.
For such students, UCLA ECE offers a broad range of courses covering every aspect of wireless systems: from theory to implementation.
We offer courses on signal processing for communications, wireless communication theory, multi-antenna communications, RF circuit design, antenna theory, information theory, communication networks, optimization, and machine learning, along with several others.
Following graduation, UCLA ECE students go on to develop cutting-edge technologies at companies such as Qualcomm, Broadcom, Viasat, Samsung, Google, Apple, Intel, Cisco, NVIDIA, Ericsson, Nokia, IBM, and countless others.

{% assign f = "ucla/07.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/13.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/12.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/23.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/02.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/35.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/10.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/04.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

{% assign f = "ucla/28.jpg" %}
{% assign c = "" %}
{% include image.html file=f caption=c link=true width=w %}

