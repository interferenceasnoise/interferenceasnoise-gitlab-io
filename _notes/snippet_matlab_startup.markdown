---
layout: note
title:  "Code Snippet: MATLAB Startup Commands"
date: 2020-11-13
categories: [matlab]
summary: A code snippet of useful MATLAB startup commands.
---

In your `startup.m` file, include the following for LaTeX-rendering and more.

{% highlight matlab %}
% Plot settings
set(groot,'defaulttextinterpreter','latex'); 
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaultPolarAxesTickLabelInterpreter','latex'); 
set(groot,'defaultLegendInterpreter','latex');
set(groot,'defaultColorbarTickLabelInterpreter','latex'); 
set(groot,'defaultAxesFontSize',12);
set(groot,'defaultLineLineWidth',2);

% Plot tools (R2018b)
set(groot,'defaultFigureCreateFcn','addToolbarExplorationButtons(gcf)');
set(groot,'defaultAxesCreateFcn','set(get(gca,''Toolbar''),''Visible'',''off'')');
{% endhighlight %}



