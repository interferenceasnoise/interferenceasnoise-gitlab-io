---
layout: note
title: "What is Full-Duplex?"
categories: [wireless,full-duplex]
date: 2023-03-15
summary: An introduction to full-duplex communication systems.
published: true
---

### Traditional Half-Duplex Systems

Until now, wireless communication systems have almost exclusively operated in a **half-duplex** fashion, where transmission and reception of radio waves have typically been separated---or orthogonalized---in the time domain or frequency domain. Put simply, signals transmitted or received by a traditional half-duplex system exist in different frequency bands or at different times, referred to as frequency-division duplexing (FDD) and time-division duplexing (TDD), respectively. 

{% assign f1 = "/fig/resource_tdd.svg" %}
{% assign f2 = "/fig/resource_fdd.svg" %}
{% assign c = "(left) TDD divides radio resources in time. (right) FDD divides radio resources in frequency." %}
{% include image-double.html file1=f1 file2=f2 width1=35 width2=35 caption=c link=false %}

### Self-Interference

Half-duplex operation is necessitated by the manifestation of **self-interference** when a transceiver attempts to receive signals while simultaneously transmitting in the same spectrum. In most cases, self-interference is many orders of magnitude stronger than a relatively weak signal-of-interest (or desired receive signal ), which has presumably propagated tens or hundreds of meters. This makes it virtually impossible to accurately recover the desired receive signal from their combination without taking additional measures to
mitigate the effects of self-interference. By receiving in neighboring frequency spectrum or on a separate time slot as its transmissions, a half-duplex transceiver can avoid inflicting self-interference onto a desired receive signal, hence the usage of FDD and TDD.

{% assign f = "/fig/fdx_si.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=75 link=false %}

By their nature, FDD and TDD both consume radio resources by dedicating time-frequency resources to either transmission or reception. Of course, this would not be an issue if practical systems were not resource-constrained. In reality, all practical wireless communication systems operate on limited time-frequency resources. At the very least, most systems are confined to certain frequency spectrum by regulatory bodies, such as the Federal Communications Commission (FCC) in the United States. The consumption of radio resources by half-duplex operation has motivated researchers to explore
in-band full-duplex operation. Starting in the late 2000s, researchers began heavily investigating and developing means to mitigate self-interference and bring full-duplex to life. Since then, full-duplex has matured and has recently found new life in mmWave networks, in joint communication and sensing, and through advancements in machine learning.

{% assign f = "/fig/resource_fdx.svg" %}
{% assign c = "Full-duplex allows a device to transmit and receive using the entire radio resources available." %}
{% include image.html file=f caption=c width=35 link=false %}

### Why Full-Duplex?

Full-duplex capability allows a transceiver to concurrently transmit and receive over the same frequency spectrum. In other words, transmission and reception can both make use of the full available frequency spectrum all the time. As mentioned, when operating in this full-duplex fashion, self-interference is inflicted onto a desired receive signal. Researchers have developed a variety of ways to mitigate self-interference using RF components, analog and digital filters, and other creative means. If self-interference can be sufficiently mitigated, a full-duplex transceiver can reliably receive while transmitting in-band, unlocking a number of enhancements.

#### Spectral Efficiency Gains

As depicted below, a base station with full-duplex capability may transmit downlink and concurrently receive uplink with a neighboring full-duplex user. This makes better use of radio resources, and as intuition may suggest, leads to a potential doubling of spectral efficiency, as compared to half-duplex techniques. In other words, radio resources are being used twice as efficiently with full-duplex, since they are used for both transmission and reception, rather than divided as with FDD and TDD, as illustrated in Figure 1. 

{% assign f = "/fig/fdx_type_07.svg" %}
{% assign c = "A full-duplex base station transmits and receives with a full-duplex user." %}
{% include image.html file=f width=40 caption=c link=false %}

The figure below depicts another full-duplex operating mode, where a full-duplex base station transmits downlink to a half-duplex user while receiving uplink from another half-duplex user. This mode of operation can also potentially double spectral efficiency. It is important to note that cross-link interference is inflicted on the downlink user by the uplink user, the level of which depends on a number of factors, chiefly the users’ locations and the environment.

{% assign f = "/fig/fdx_type_08.svg" %}
{% assign c = "A full-duplex base station transmits to one user while receiving from another user in-band." %}
{% include image.html file=f width=75 caption=c link=false %}


#### Medium Access and Spectrum Sharing
Half-duplex transceivers have been ubiquitous in wireless networks, and for good reason, communication standards and practices have been built on this half-duplex assumption.
The ability to transmit and receive simultaneously and in-band is a transformative technology that can unlock new approaches to medium access and spectrum sharing that are more efficient than those built on a half-duplex assumption.
In turn, full-duplex can facilitate wireless networks that deliver higher throughput, inflict lower interference, and make better use of spectrum.

**Overcoming the Hidden Node Problem.**
To illustrate this, consider the famous hidden node problem in wireless networks:
if two users are distant from one another but both within earshot of a nearby access point, the two users may be unaware when the other is transmitting to the access point.
This can lead to collisions at the access point---and hence a waste of radio resources---if not dealt with.
Conventional approaches to overcome this use handshaking between users and the access point to grant a user permission to transmit (e.g., request-to-send (RTS) and clear-to-send (CTS) mechanisms) along with random backoffs.
By upgrading the access point with full-duplex capability, more efficient approaches to medium access can be employed.
For instance, as illustrated in , the full-duplex access point can broadcast a busy tone anytime it is receiving from a user.
This busy tone can be heard by all users in the network, informing them to not transmit.
Compared to conventional approaches to medium access, this approach consumes no additional radio resources and avoids the overhead associated with handshaking between users and the access point and reduces latency.

{% assign f = "/fig/mac_type_01.svg" %}
{% assign c = "While receiving from one user, a full-duplex access point broadcasts a busy tone instructing all other users not to transmit, preventing collisions and overcoming the hidden node problem." %}
{% include image.html file=f width=75 caption=c link=false %}

**Dynamic Spectrum Access and Cognitive Radios.**
The number of devices with wireless connectivity has grown profoundly over the past two decades and will seemingly continue for years to come.
The amount of available spectrum has not grown at a comparable pace, however.
In light of this, researchers have proposed spectrum sharing and cognitive radios to dynamically and opportunistically make better use of frequency spectrum when it is free.
For instance, a cognitive radio may sense a frequency band to see if it is being used by nearby devices.
If there appears to be no activity, the cognitive radio may begin transmitting information.
Periodically, the cognitive radio may halt transmission to sense the channel to ensure that it does not inflict interference on incumbents that have rights to the band---a waste of resources if none are detected.
Full-duplex can make this process more efficient by empowering the cognitive radio to continuously sense the channel while transmitting.
This allows it to more efficiently transmit information, since it does not have to halt transmission to sense the channel, and enables the cognitive radio to react more quickly to the presence of incumbents.
Without full-duplex capability, the cognitive radio would presumably be unaware of the presence of incumbents until the end of its transmission, potentially causing interference that leads to collisions.
Techniques discussed for overcoming the hidden node problem can be applied in this setting, as well, to instruct nearby cognitive radios to not transmit.

**Channel Sensing to Reduce the Effects of Interference.**
As a final example of full-duplex applied to medium access, we consider the case where an access point serves users in the presence of neighboring nodes that may inflict interference.
For instance, one can consider two Wi-Fi access points operating independently but near one another.
When one access point transmits to a user, successful reception at that user may be corrupted by neighboring interference, requiring the access point to retransmit the data.
Naturally, this consumes radio resources and can lead to delays in communication.
With full-duplex capability, the access point may sense the channel while transmitting, allowing it to potentially halt transmission to avoid collisions caused by neighboring interference and subsequently redirect resources to another user that may not be impacted by this interference.
It is important to note that the requirements on mitigating \si are stricter when decoding data carried by a desired receive signal, compared to those for channel sensing, which is often merely detecting energy levels in a particular frequency band.

{% assign f = "/fig/mac_type_04.svg" %}
{% assign c = "An access point equipped with full-duplex capability can sense the channel while transmitting, allowing it to reroute resources in the presence of interference that would otherwise cause collisions." %}
{% include image.html file=f width=75 caption=c link=false %}


#### Network-Level Enhancements

Upgrading transceivers with full-duplex capability can be felt at the network level in a few ways.
Even when only some devices are equipped with full-duplex---while the remainder are half-duplex-capable---a wireless network can enjoy improvements in throughput and latency.
In fact, network throughput can magnify beyond the doubling of spectral efficiency we are familiar with at the link level with full-duplex.
This can be attributed to the fact that full-duplex can reduce multiplexing delays, reduce overhead associated with medium access control, and enable new ways to manage interference---all of which can improve network throughput.
There are applications of full-duplex technology beyond what was highlighted herein, such as in joint communication and sensing, secure communications, military communications, radar, and
more.

### Additional Readings

Here are a few good papers to read more on full-duplex:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf)
- [In-Band Full-Duplex Wireless: Challenges and Opportunities](https://ieeexplore.ieee.org/document/6832464)
- [Full-Duplex Transceiver System Calculations: Analysis of ADC and Linearity Challenges](https://ieeexplore.ieee.org/document/6782415)
- [Full-Duplex Mobile Device: Pushing the Limits](https://ieeexplore.ieee.org/document/7565192)
- [Widely Linear Digital Self-Interference Cancellation in Direct-Conversion Full-Duplex Transceiver](https://ieeexplore.ieee.org/document/6832439)
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf)

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
