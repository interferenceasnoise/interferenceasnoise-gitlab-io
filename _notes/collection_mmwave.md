---
layout: note
title: "Millimeter Wave Collection"
categories: [collection]
date: 2023-03-15
summary: A list of all notes on the topic of mmWave communication.
published: true
---

Below is a list of notes directly related to millimeter wave communication systems.
It is suggested that they be read in the order listed below.

{% assign notes = site.notes %}

{% for note in notes %}
{% if note.categories contains 'mmwave' %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
{% endif %}
{% endfor %}

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
