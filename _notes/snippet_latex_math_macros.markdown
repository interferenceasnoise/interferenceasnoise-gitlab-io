---
layout: note
title:  "Code Snippet: LaTeX Math Macros"
date: 2020-11-14
categories: [latex]
summary: A code snippet of useful LaTeX math macros.
---


### Preliminaries

The following packages are necessary for some of the macros listed here.

{% highlight latex %}
\usepackage{amssymb}
\usepackage{xspace}
\usepackage{bm}
{% endhighlight %}


### Typesetting

{% highlight latex %}
\newcommand{\set}[1]{\ensuremath{\mathcal{#1}}\xspace} % sets
\newcommand{\mat}[1]{\ensuremath{\mathbf{#1}}\xspace} % matrices
\renewcommand{\vec}[1]{\ensuremath{\mathbf{#1}}\xspace} % vectors
{% endhighlight %}


### Enclosures

{% highlight latex %}
\newcommand{\parens}[1]{\ensuremath{\left(#1\right)}\xspace}
\newcommand{\brackets}[1]{\ensuremath{\left[#1\right]}\xspace}
\newcommand{\braces}[1]{\ensuremath{\left\{#1\right\}}\xspace}
\newcommand{\bars}[1]{\ensuremath{\left\vert#1\right\vert}\xspace}
\newcommand{\doublebars}[1]{\ensuremath{\left\Vert#1\right\Vert}\xspace}
\newcommand{\angles}[1]{\ensuremath{\left\langle#1\right\rangle}\xspace}
{% endhighlight %}


### Basic Stuff

{% highlight latex %}
\newcommand{\real}{\ensuremath{\mathbb{R}}\xspace}
\newcommand{\complex}{\ensuremath{\mathbb{C}}\xspace}
\renewcommand{\natural}{\ensuremath{\mathbb{N}}\xspace}
\newcommand{\integer}{\ensuremath{\mathbb{Z}}\xspace}
\renewcommand{\j}{\ensuremath{\mathrm{j}}}
\newcommand{\e}{\ensuremath{\mathrm{e}}}
\newcommand{\ceil}[1]{\ensuremath{\left\lceil{#1}\right\rceil}\xspace}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor{#1}\right\rfloor}\xspace}
\newcommand{\defeq}{\ensuremath{\triangleq}\xspace}
{% endhighlight %}



### Sets

{% highlight latex %}
% SET OPERATORS
\newcommand{\card}[1]{\bars{#1}}

% SCALAR/GENERIC SETS
\newcommand{\setreal}{\ensuremath{\real}}
\newcommand{\setcomplex}{\ensuremath{\complex}}
\newcommand{\setnatural}{\ensuremath{\natural}}
\newcommand{\setinteger}{\ensuremath{\integer}}

% SPECIAL REAL SETS
\newcommand{\setrealpos}{\ensuremath{\setreal_{++}}}
\newcommand{\setrealnonneg}{\ensuremath{\setreal_{+}}}
\newcommand{\setrealneg}{\ensuremath{\setreal_{--}}}
\newcommand{\setrealnonpos}{\ensuremath{\setreal_{-}}}

% SPECIAL COMPLEX SETS
\newcommand{\setcomplexcircle}[1]{\ensuremath{\setcomplex_{#1}}}
\newcommand{\setcomplexcircleunit}{\setcomplexcircle{1}}
\newcommand{\setcomplexdisc}[1]{\setcomplex_{\leq#1}}
\newcommand{\setcomplexdiscunit}{\setcomplexdisc{1}}

% SPECIAL NATURAL SETS
\newcommand{\setnaturalpos}{\ensuremath{\setnatural_{++}}}
\newcommand{\setnaturalnonneg}{\ensuremath{\setnatural_{+}}}

% SPECIAL INTEGER SETS
\newcommand{\setintegerpos}{\ensuremath{\setinteger_{++}}}
\newcommand{\setintegernonneg}{\ensuremath{\setinteger_{+}}}
\newcommand{\setintegerneg}{\ensuremath{\setinteger_{--}}}
\newcommand{\setintegernonpos}{\ensuremath{\setinteger_{-}}}

% VECTOR SETS
\newcommand{\setvector}[2]{\ensuremath{#1^{#2 \times 1}}\xspace}
\newcommand{\setvectorreal}[1]{\setvector{\setreal}{#1}}
\newcommand{\setvectorcomplex}[1]{\setvector{\setcomplex}{#1}}

% MATRIX SETS
\newcommand{\setmatrix}[3]{\ensuremath{#1^{#2 \times #3}}\xspace}
\newcommand{\setmatrixreal}[2]{\setmatrix{\setreal}{#1}{#2}}
\newcommand{\setmatrixcomplex}[2]{\setmatrix{\setcomplex}{#1}{#2}}

% SPECIAL MATRIX SETS
\newcommand{\setsymmetric}{\ensuremath{\mathbb{S}}\xspace}
\newcommand{\setsymmetricpsd}{\ensuremath{\setsymmetric_{+}}}
\newcommand{\setsymmetricpd}{\ensuremath{\setsymmetric_{++}}}
\newcommand{\setsymmetricnsd}{\ensuremath{\setsymmetric_{-}}}
\newcommand{\setsymmetricnd}{\ensuremath{\setsymmetric_{--}}}

\newcommand{\setmatrixsymmetric}[1]{\setmatrix{\setsymmetric}{#1}{#1}}
\newcommand{\setmatrixsymmetricpsd}[1]{\setmatrix{\setsymmetricpsd}{#1}{#1}}
\newcommand{\setmatrixsymmetricpd}[1]{\setmatrix{\setsymmetricpd}{#1}{#1}}
\newcommand{\setmatrixsymmetricnsd}[1]{\setmatrix{\setsymmetricnsd}{#1}{#1}}
\newcommand{\setmatrixsymmetricnd}[1]{\setmatrix{\setsymmetricnd}{#1}{#1}}
{% endhighlight %}




### Special Vectors and Matrices

{% highlight latex %}
\newcommand{\zerovec}{\ensuremath{\vec{0}}\xspace}
\newcommand{\onevec}[1]{\ensuremath{\vec{1}_{#1}}\xspace}
\newcommand{\zeromat}{\ensuremath{\mat{0}}\xspace}
\newcommand{\eyemat}[1]{\ensuremath{\mat{I}_{#1}}\xspace}
{% endhighlight %}


### Linear Algebra Functions and Operations

{% highlight latex %}
\newcommand{\inv}{\ensuremath{^{-1}}\xspace}
\newcommand{\pinv}{\ensuremath{^\dagger}\xspace}
\newcommand{\trans}{\ensuremath{^{\mathrm{T}}}\xspace}
\newcommand{\ctrans}{\ensuremath{^{*}\xspace}
\newcommand{\conj}{\ensuremath{^{\mathrm{c}}}\xspace} % element-wise conjugate
\newcommand{\norm}[1]{\doublebars{#1}}
\newcommand{\trace}[1]{\ensuremath{\mathrm{trace}\parens{#1}}\xspace}
\newcommand{\vectorize}[1]{\ensuremath{\mathrm{vec}\parens{#1}}\xspace}
\newcommand{\kron}{\ensuremath{\otimes}\xspace}
\newcommand{\had}{\ensuremath{\odot}\xspace}
\newcommand{\entry}[2]{\ensuremath{\brackets{#1}_{#2}}\xspace}
\newcommand{\rank}[1]{\ensuremath{\mathrm{rank}\parens{#1}}}
\newcommand{\lnop}[1]{\ensuremath{\mathrm{ln}\parens{#1}}}
\newcommand{\logop}[1]{\ensuremath{\mathrm{log}\parens{#1}}}
\newcommand{\logdet}[1]{\ensuremath{\mathrm{log}\bars{#1}}}
\newcommand{\logtwodet}[1]{\ensuremath{\mathrm{log}_{2}\bars{#1}}}
\newcommand{\loge}[1]{\ensuremath{\mathrm{log}_{\e}\parens{#1}}}
\newcommand{\logtwo}[1]{\ensuremath{\mathrm{log}_{2}\parens{#1}}}
\newcommand{\logten}[1]{\ensuremath{\mathrm{log}_{10}\parens{#1}}}
\newcommand{\diag}[1]{\ensuremath{\mathrm{diag}\parens{#1}}}
\newcommand{\zeros}[1]{\ensuremath{\mathrm{zeros}\parens{#1}}}
\newcommand{\ones}[1]{\ensuremath{\mathrm{ones}\parens{#1}}}
\newcommand{\abs}[1]{\ensuremath{\mathrm{abs}\parens{#1}}}
\newcommand{\angleop}[1]{\ensuremath{\textrm{angle}\parens{#1}}}
\newcommand{\col}[1]{\ensuremath{\mathrm{col}\parens{#1}}\xspace}
\newcommand{\colspan}[1]{\ensuremath{\mathrm{span}\parens{#1}}\xspace}
\newcommand{\nullspace}[1]{\ensuremath{\mathrm{null}\parens{#1}}\xspace}
\newcommand{\kernel}[1]{\ensuremath{\mathrm{ker}\parens{#1}}\xspace}
{% endhighlight %}


### Singular Values and Eigenvalues

{% highlight latex %}
\newcommand{\svmin}[1]{\ensuremath{\sigma_{\mathrm{min}}\parens{#1}}\xspace}
\newcommand{\svmax}[1]{\ensuremath{\sigma_{\mathrm{max}}\parens{#1}}\xspace}
\newcommand{\svminsq}[1]{\ensuremath{\sigma_{\mathrm{min}}^2\parens{#1}}\xspace}
\newcommand{\svmaxsq}[1]{\ensuremath{\sigma_{\mathrm{max}}^2\parens{#1}}\xspace}

\newcommand{\evmin}[1]{\ensuremath{\lambda_{\mathrm{min}}\parens{#1}}\xspace}
\newcommand{\evmax}[1]{\ensuremath{\lambda_{\mathrm{max}}\parens{#1}}\xspace}
\newcommand{\evminsq}[1]{\ensuremath{\lambda_{\mathrm{min}}^2\parens{#1}}\xspace}
\newcommand{\evmaxsq}[1]{\ensuremath{\lambda_{\mathrm{max}}^2\parens{#1}}\xspace}
{% endhighlight %}



### Norms

{% highlight latex %}
\newcommand{\pnorm}[2]{\ensuremath{\doublebars{#2}_{#1}}\xspace}
\newcommand{\normzero}[1]{\pnorm{0}{#1}}
\newcommand{\normone}[1]{\pnorm{1}{#1}}
\newcommand{\normtwo}[1]{\pnorm{2}{#1}}
\newcommand{\norminf}[1]{\pnorm{\infty}{#1}}
\newcommand{\normnuc}[1]{\pnorm{*}{#1}}
\newcommand{\normfro}[1]{\pnorm{\mathrm{F}}{#1}}
{% endhighlight %}


### Distributions and Probability

{% highlight latex %}
\newcommand{\distgauss}[2]{\ensuremath{\mathcal{N}\parens{#1,#2}}\xspace} % Gaussian distribution
\newcommand{\distcgauss}[2]{\ensuremath{\mathcal{N}_{\complex}\parens{#1,#2}}\xspace} % complex Gaussian 
\newcommand{\distuniform}[2]{\ensuremath{\mathrm{Unif}\parens{#1,#2}}\xspace} % Uniform
\newcommand{\distexp}[1]{\ensuremath{\mathrm{Exp}\parens{#1}}\xspace} % Exponential
\newcommand{\distlaplace}[2]{\ensuremath{\mathrm{Laplace}\parens{#1,#2}}\xspace} % Laplace

\newcommand{\ev}[1]{\ensuremath{\mathbb{E}\brackets{#1}}\xspace}
\newcommand{\var}[1]{\ensuremath{\mathrm{var}\parens{#1}}\xspace}
{% endhighlight %}


### Definiteness

{% highlight latex %}
\newcommand{\ispsd}{\ensuremath{\succeq}}
\newcommand{\ispd}{\ensuremath{\succ}}
\newcommand{\isnsd}{\ensuremath{\preceq}}
\newcommand{\isnd}{\ensuremath{\prec}}
\newcommand{\isnotpsd}{\ensuremath{\nsucceq}}
\newcommand{\isnotpd}{\ensuremath{\nsucc}}
\newcommand{\isnotnsd}{\ensuremath{\npreceq}}
\newcommand{\isnotnd}{\ensuremath{\nprec}}
{% endhighlight %}


### Optimization

{% highlight latex %}
\newcommand{\argmin}{\ensuremath{\mathrm{argmin}}\xspace}
\newcommand{\argmax}{\ensuremath{\mathrm{argmax}}\xspace}
\newcommand{\mink}{\ensuremath{\mathrm{mink}}\xspace}
\newcommand{\maxk}{\ensuremath{\mathrm{maxk}}\xspace}
\newcommand{\maxop}[1]{\ensuremath{\mathrm{max}\parens{#1}}\xspace}
\newcommand{\maxkop}[1]{\ensuremath{\mathrm{maxk}\parens{#1}}\xspace}
\newcommand{\minop}[1]{\ensuremath{\mathrm{min}\parens{#1}}\xspace}
\newcommand{\minkop}[1]{\ensuremath{\mathrm{mink}\parens{#1}}\xspace}
\newcommand{\st}{\ensuremath{\mathrm{s.t.~}}\xspace}
\newcommand{\opt}{\ensuremath{^{\star}}\xspace}
{% endhighlight %}


### Decibels

{% highlight latex %}
\newcommand{\todB}[1]{\ensuremath{\brackets{#1}_{\mathrm{dB}}}}
\newcommand{\todBm}[1]{\ensuremath{\brackets{#1}_{\mathrm{dBm}}}}
{% endhighlight %}



### Special Variables

{% highlight latex %}
\newcommand{\snr}{\ensuremath{\mathrm{SNR}}\xspace}
\newcommand{\sqnr}{\ensuremath{\mathrm{SQNR}}\xspace}
\newcommand{\sqr}{\ensuremath{\mathrm{SQR}}\xspace}
\newcommand{\qnr}{\ensuremath{\mathrm{QNR}}\xspace}
\newcommand{\sinr}{\ensuremath{\mathrm{SINR}}\xspace}
\newcommand{\siqnr}{\ensuremath{\mathrm{SIQNR}}\xspace}
\newcommand{\sir}{\ensuremath{\mathrm{SIR}}\xspace}
\newcommand{\inr}{\ensuremath{\mathrm{INR}}\xspace}

\newcommand{\mse}{\ensuremath{\mathrm{MSE}}\xspace}
\newcommand{\eirp}{\ensuremath{\mathrm{EIRP}}\xspace}
\newcommand{\enob}{\ensuremath{\mathrm{ENOB}}\xspace}
\newcommand{\papr}{\ensuremath{\mathrm{PAPR}}\xspace}
{% endhighlight %}


# Vector Shorthand

{% highlight latex %}
\def\va{\vec{a}}
\def\vb{\vec{b}}
\def\vc{\vec{c}}
\def\vd{\vec{d}}
\def\ve{\vec{e}}
\def\vf{\vec{f}}
\def\vg{\vec{g}}
\def\vh{\vec{h}}
\def\vi{\vec{i}}
\def\vj{\vec{j}}
\def\vk{\vec{k}}
\def\vl{\vec{l}}
\def\vm{\vec{m}}
\def\vn{\vec{n}}
\def\vo{\vec{o}}
\def\vp{\vec{p}}
\def\vq{\vec{q}}
\def\vr{\vec{r}}
\def\vs{\vec{s}}
\def\vt{\vec{t}}
\def\vu{\vec{u}}
\def\vv{\vec{v}}
\def\vw{\vec{w}}
\def\vx{\vec{x}}
\def\vy{\vec{y}}
\def\vz{\vec{z}}

\def\vmu{\vec{\mu}}
\def\vtheta{\vec{\theta}}
\def\vphi{\vec{\phi}}
\def\vpsi{\vec{\psi}}
{% endhighlight %}


# Matrix Shorthand

{% highlight latex %}
\def\mA{\mat{A}}
\def\mB{\mat{B}}
\def\mC{\mat{C}}
\def\mD{\mat{D}}
\def\mE{\mat{E}}
\def\mF{\mat{F}}
\def\mG{\mat{G}}
\def\mH{\mat{H}}
\def\mI{\mat{I}}
\def\mJ{\mat{J}}
\def\mK{\mat{K}}
\def\mL{\mat{L}}
\def\mM{\mat{M}}
\def\mN{\mat{N}}
\def\mO{\mat{O}}
\def\mP{\mat{P}}
\def\mQ{\mat{Q}}
\def\mR{\mat{R}}
\def\mS{\mat{S}}
\def\mT{\mat{T}}
\def\mU{\mat{U}}
\def\mV{\mat{V}}
\def\mW{\mat{W}}
\def\mX{\mat{X}}
\def\mY{\mat{Y}}
\def\mZ{\mat{Z}}

\def\mBeta{\mat{\beta}
\def\mDelta{\mat{\Delta}}
\def\mGamma{\mat{\Gamma}}
\def\mLambda{\mat{\Lambda}}
\def\mOmega{\mat{\Omega}}
\def\mPi{\mat{\Pi}}
\def\mPsi{\mat{\Psi}}
\def\mPhi{\mat{\Phi}}
\def\mSigma{\mat{\Sigma}}
\def\mTheta{\mat{\Theta}}
\def\mUpsilon{\mat{\Upsilon}}
\def\mXi{\mat{\Xi}}
{% endhighlight %}




