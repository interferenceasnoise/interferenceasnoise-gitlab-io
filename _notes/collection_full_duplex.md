---
layout: note
title: "Full-Duplex Collection"
categories: [collection]
date: 2023-03-15
summary: A list of all notes on the topic of full-duplex.
published: true
---

Below is a list of notes directly related to full-duplex.
It is suggested that they be read in the order listed below.

{% assign notes = site.notes %}

{% for note in notes %}
{% if note.categories contains 'full-duplex' %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
{% endif %}
{% endfor %}

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
