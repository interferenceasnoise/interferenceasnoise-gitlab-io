---
layout: note
title:  "A LaTeX Poster Template"
date: 2022-06-07
categories: [latex]
summary: A LaTeX template for posters.
---

The LaTeX poster template can be found on Overleaf at [this link](https://www.overleaf.com/read/vmmyxfnnsnnq).

This is [an example](/pdf/poster_example.pdf) of the poster output.



