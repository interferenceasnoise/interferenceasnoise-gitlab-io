---
layout: note
title: "The Connection Between Multiple Defintions of SNR"
categories: [wireless]
date: 2021-03-03
summary: Clarifying remarks on SNR.
published: true
---


As simple as it may seem, SNR confused me at first far more than it should have. Hopefully this will help you avoid that same confusion.




SNR can be written simply as




$$ \mathrm{SNR} = \frac{P_{\mathrm{rx}}}{P_{\mathrm{noise}}} $$




which often takes the form (1) as




$$ \mathrm{SNR} = \frac{P_{\mathrm{tx}} \cdot G^2 \cdot \chi \cdot |h|^2}{N_0 \cdot B} = \frac{P_{\mathrm{tx}} \cdot G^2 \cdot \chi \cdot |h|^2}{N_0 \cdot B} $$




or the similar form (2) as




$$ \mathrm{SNR} = \frac{E_{\mathrm{s}} \cdot G^2 \cdot \chi \cdot |h|^2}{N_0}  $$




So what's the difference? How are these two forms equivalent?




I like to think of form (1) as being "watts over watts". This is the received power (in watts) relative to the noise power (in watts).




I like to think of form (2) as being "joules per symbol over joules per symbol". This is the received energy per symbol (joules) relative to the noise energy per symbol (in joules).




Consider the noise power spectral density $$N_0$$, which has units power/Hz (e.g., watts/Hz or dBm/Hz). $$N_0 = -174$$ dBm/Hz, for example.




When we assume our transmissions have symbol period $$T$$ and bandwidth $$B = 1/T$$, we can see that $$N_0 \cdot B$$ is simply the integrated noise over our bandwidth (in watts or dBm).




As evidenced by form (1), we must compare our received power (in watts or dBm) versus this integrated noise power (in watts or dBm).




In form (2), we compare the received energy per symbol (in joules) versus the noise energy per symbol (in joules). The average noise energy per symbol is simply $$N_0$$.




The average energy per symbol $$E_{\mathrm{s}}$$ can be found by multiplying our transmit power (joules per second) by the symbol period (seconds per symbol). Equivalently, this can be thought of as spreading out our total transmit power over our bandwidth $$B$$.




So, we have 




$$E_{\mathrm{s}} = P_{\mathrm{tx}} \cdot T = P_{\mathrm{tx}} / B$$


Sometimes, we are not concerned with specifying a bandwidth $$B$$, leading people use form (2) with $$N_0$$ instead of $$N_0 \cdot B$$ and $$E_{\mathrm{s}}$$ instead of $$P_{\mathrm{tx}}$$. Beware, however, sometimes people will write $$E_{\mathrm{s}}$$ as $$P_{\mathrm{tx}}$$ since it more easily communicates the "power" gain that is applied at the transmitter. In such cases, $$P_{\mathrm{tx}}$$ would be in units of joules per symbol, not watts! Think about it! You can't compare a transmit power of 30 dBm to a noise floor of -174 dBm/Hz! That's crazy!

In many contexts, we may want to exclude small-scale fading from our definition of SNR so that we may use it as a scalar quantity. In such cases, we refer to $$\mathrm{SNR}$$ as the large-scale SNR (sometimes average SNR), not the instantaneous SNR, which can be written without the effects of $$ \lvert h \rvert^2 $$ as

$$ \mathrm{SNR} = \frac{P_{\mathrm{tx}} \cdot G^2 \cdot \chi}{N_0 \cdot B} = \frac{E_{\mathrm{s}} \cdot G^2 \cdot \chi}{N_0} $$

Sometimes, the large-scale fading term $$\chi$$ is included into gain $$G^2$$ to leave us with simply

$$ \mathrm{SNR} = \frac{P_{\mathrm{tx}} \cdot G^2}{N_0 \cdot B} = \frac{E_{\mathrm{s}} \cdot G^2}{N_0} $$
