---
layout: note
title: "Analog and Digital Self-Interference Cancellation"
categories: [wireless,full-duplex]
date: 2023-03-15
summary: "An overview of established full-duplex solutions: analog and digital self-interference cancellation."
summary-extended: >
   Self-interference cancellation is essential to enable in-band full-duplex operation. Digital self-interference cancellation is an effective and flexible route to enabling full-duplex, but it is bottlenecked in practice by imperfections and limitations of hardware. As such, it is often used in conjunction with analog self-interference cancellation, which can inherently account for hardware impairments, relax the requirements of digital self-interference cancellation, and prevent self-interference from saturating receive-chain components. Circulators and other duplexers can provide isolation between the transmitter and receiver of a full-duplex transceiver when using a single antenna, effectively weakening self-interference that analog and digital self-interference cancellation must tackle.
published: true
---

Before reading this note, it is recommended to first read the following note:
- [What is Full-Duplex?](/notes/full_duplex)

### Self-Interference Cancellation to Enable Full-Duplex Operation

Until now, wireless communication systems have almost exclusively operated in a **half-duplex** fashion, where transmission and reception of radio waves have typically been separated---or orthogonalized---in the time domain or frequency domain. Put simply, signals transmitted or received by a traditional half-duplex system exist in different frequency bands or at different times, referred to as frequency-division duplexing (FDD) and time-division duplexing (TDD), respectively. 

{% assign f = "/fig/fdx_si.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=75 link=false %}

Successfully equipping a device with full-duplex capability relies on mitigating---or cancelling---self-interference to levels that are sufficiently low.
The amount of **self-interference cancellation** needed depends on the particular application.
In most settings, full-duplex solutions aim to cancel self-interference to near or below the receiver noise floor.
This ensures that the full-duplex resource gains are not eroded by the presence of high self-interference.
The **residual** self-interference is that which remains after efforts of self-interference cancellation.

### Two-Staged Approach: Analog and Digital Cancellation

In this note, we outline methods of self-interference cancellation in both the analog and digital domains.
Regardless of domain, the motivation behind self-interference cancellation can largely be summarized as leveraging the fact that a transceiver has knowledge of its own transmitted signal and can therefore potentially reconstruct self-interference and subtract it from the received signal, leaving the desired portion virtually free of self-interference.
In many cases, a staged approach to self-interference cancellation is employed as illustrated in the figure below, where a portion of self-interference is cancelled using an analog filter and a significant portion of the remainder is cancelled using digital filtering.

{% assign f = "/fig/sic_type_06.svg" %}
{% assign c = "A received signal undergoes analog and digital self-interference cancellation before undergoing conventional receive processing to recover desired receive data." %}
{% include image.html file=f caption=c width=95 link=false %}

This staged approach is also depicted in the figure below, where a full-duplex transceiver with separate antennas for transmission and reception employs an analog self-interference cancellation filter between its antennas at radio frequency (RF) and a digital self-interference cancellation filter.

{% assign f = "/fig/fdx_sub6_siso_asic_dsic.svg" %}
{% assign c = "Self-interference manifests when a full-duplex transceiver attempts to simultaneously transmit and receive using the same frequency spectrum. Here, separate antennas are used for transmission and reception, with analog and digital self-interference cancellation used to reconstruct and subsequently cancel self-interference incurred at the receiver." %}
{% include image.html file=f caption=c width=75 link=false %}

### Analog Self-Interference Cancellation

As illustrated in the figure below, analog self-interference cancellation typically exists as a digitally-controlled analog filter placed between the transmitter and receiver of a full-duplex transceiver.
Analog self-interference cancellation filters come in many forms, existing as RF, intermediate frequency, and baseband circuitry, and even as optical filters.
Analog self-interference cancellation is often driven by the tapping off a small portion of the upconverted RF transmit signal.
This transmit signal undergoes filtering within the analog self-interference cancellation filter before being injected at the receiver.
The injected signal is an inverted reconstruction of self-interference, which, when combined with the received signal, destructively combines with self-interference.
After this combining, there is some degree of residual self-interference due to imperfect reconstruction, which may stem from estimation errors, hardware limitations, and hardware imperfections.
By tapping off the transmit signal after the transmit chain, analog self-interference cancellation will inherently incorporate transmit-side impairments unbeknownst to baseband, such as power amplifier (PA) nonlinearities, which have proven to be a dominant factor in self-interference cancellation.
Other approaches, sometimes called digitally-assisted approaches, use a dedicated transmit chain to drive analog self-interference cancellation, as opposed to tapping off the transmit signal directly.
This approach cannot as well capture transmit-side impairments present in self-interference, however, since this second transmit chain naturally will not contain all artifacts of the true transmit chain.

{% assign f = "/fig/asic_type_02.svg" %}
{% assign c = "An example analog self-interference cancellation filter (an $N$-tap finite impulse response filter) with tunable tap weights $\{x_i\}$ and fixed, uniform tap delay $\tau$." %}
{% include image.html file=f caption=c width=95 link=false %}

Tuning an analog self-interference cancellation filter to effectively cancel self-interference largely consists of measuring self-interference and then configuring the filter to reconstruct its inverse.
Analog self-interference cancellation can be implemented as a time-domain filter or as a frequency-domain filter, meaning particular methods may vary but all tackle the same goal of reconstructing self-interference.
One method of time-domain analog self-interference cancellation is to estimate the impulse response of the self-interference channel and then configure the analog filter to produce this (inverted) impulse response estimate, effectively equalizing self-interference.
Estimation of the self-interference channel is typically executed by transmitting a pilot signal during a **quiet period**, when no desired receive signal is present.
One difficulty with practically executing this method lay in the fact that estimation of the self-interference channel takes place digitally, meaning estimation of the channel of interest for analog self-interference cancellation may be complicated by artifacts of the transmit and receive chains before and after analog self-interference cancellation.
This can be further complicated by the fact that an analog filter may not have an ideal impulse response itself, making it difficult to reliably produce the desired impulse response.

While it may seem fairly straightforward to implement analog self-interference cancellation, it is practically quite challenging in most cases, especially outside of well-controlled lab settings.
Most notably, there is small margin for error in self-interference cancellation due to the overwhelming strength of self-interference, reinforcing the need for extremely accurate, adaptable, and low-overhead self-interference reconstruction.
Another challenging aspect is the need to miniaturize analog self-interference cancellation filters into form-factors that integrate into devices such as cell phones, laptops, wireless routers, base stations, and the like.
Miniaturization is especially challenging in settings where the delay spread of self-interference has the potential to be high, since propagation delays need to be physically realized within the analog self-interference cancellation filter.

### Digital Self-Interference Cancellation

Cancelling self-interference through digital signal processing is naturally an attractive option in addition to analog self-interference cancellation.
The flexibility and sophistication of digital filtering can be applied to estimate and cancel self-interference and has had impressive success.
As depicted in the figures above, digital self-interference cancellation is executed after analog self-interference cancellation and therefore aims to cancel residual self-interference that remains after prior efforts of cancellation.
Naturally, one may ask whether digital self-interference cancellation can cancel all self-interference, rendering analog self-interference cancellation unnecessary. 
In general, this is not possible for a few reasons, stemming from hardware limitations and nonidealities.


**Limited Dynamic Range of Analog-to-Digital Converters.**
With reasonable resolution and appropriate gain control before analog-to-digital conversion, quantization noise is rarely an issue in traditional half-duplex systems.
In full-duplex systems, on the other hand, the strength of self-interference has the potential to **saturate** analog-to-digital converters (ADCs), even with ideal automatic gain control (AGC) and a reasonable number of bits. 
Since AGC acts on the combination of a desired signal plus self-interference and noise at the ADC input, the strength of quantization noise is dictated largely by that of self-interference.
In such cases, only a portion of the ADC's total dynamic range is effectively used to quantize a desired signal.
Consequently, even if self-interference could be completely reconstructed and cancelled digitally, its effects may remain in the form of increased quantization noise, which can severely and irreversibly degrade the quality of a desired receive signal.
This reinforces the need to sufficiently cancel self-interference before it reaches the ADC input, which is often most reliably done via analog self-interference cancellation.

{% assign f = "/fig/signals_desired_plus_interference_type_04.svg" %}
{% assign c = "Self-interference combines with a desired receive signal at each receive antenna of the full-duplex transceiver. When self-interference is much stronger than the desired receive signal, it can saturate components that have a limited dynamic range, such LNAs and ADCs. This saturation should be prevented to ensure the desired signal can be received reliably." %}
{% include image.html file=f caption=c width=85 link=false %}

**Transceiver Nonidealities.**
Practical transceivers introduce nonidealities in the transmit and receive chains, such as amplifier nonlinearities, I/Q imbalance, transmitter thermal noise, and phase noise, which complicate digital self-interference cancellation since the digital domain does not have knowledge of these imperfections.
When these nonidealities are not negligible, this requires digital self-interference cancellation to accurately estimate and subsequently cancel them, which can be computationally complex.
To reduce this burden, analog self-interference cancellation can be well positioned to cancel transmit-side impairments using the RF transmit signal as input to its cancellation filter, which inherently will include nonidealities introduced by transmit \pas and transmit thermal noise, for example.
In addition, analog self-interference cancellation can importantly ensure that the power of residual self-interference is sufficiently low such that it does not overwhelm and saturate receive-chain components such as low-noise amplifiers (LNAs), which practically have a limited dynamic range that can be exceeded by self-interference.

{% assign f = "/fig/lna_gain_compression.svg" %}
{% assign c = "The transfer function of a practical LNA exhibiting gain compression. Amplifier gain is constant for low-power input signals, but beyond a certain input power, the amplifier is unable to maintain this gain. Strong self-interference can cause the input signal power to to exceed this." %}
{% include image.html file=f caption=c width=65 link=false %}

**Recent Breakthroughs using Machine Learning.**
In addition to classical signal processing approaches for digital self-interference cancellation, solutions based on machine learning have been gaining traction and have shown impressive results.
The main motivation for the use of machine learning over classical approaches for digital self-interference cancellation is to capture transceiver nonidealities with reduced complexity.
Classical signal processing approaches have been able to effectively estimate and account for transceiver impairments when reconstructing and subsequently cancelling self-interference.
This is done by modeling transceiver impairments with established, parameterized models, but the estimation of model parameters is computationally expensive with classical methods. 
Machine learning has shown to be able to offer comparable performance as classical methods in capturing transceiver impairments when reconstructing self-interference but with reduced complexity.
Experimental validation of these digital self-interference cancellation solutions based on machine learning has proven their effectiveness.
In addition, machine learning can be used to reduce the complexity of self-interference cancellation in multi-antenna systems.
Rather than merely replicating single-antenna self-interference cancellation solutions as an extension to multi-antenna systems, researchers have shown that machine learning can reduce the size and complexity of digital self-interference cancellation by learning correlations between antennas.


### Circulators and Antenna Isolation 
An RF component known as a **circulator** has been used in many monostatic radar and communication applications as a duplexer when a single antenna is used for simultaneous transmission and reception of RF signals. 
In its simplest form, a circulator is a three-port, passive device where a signal entering a given port is "circulated" to the next port in the rotation. 
An example of this device being used with a single antenna shared by transmission and reception can be seen in the figure below. 
Transmit signals enter port 1 of the circulator and exit at port 2, where they are radiated by the antenna.
Signals received by the antenna enter port 2 and are circulated to port 3, where they exit the circulator and enter the receive chain. 
This establishes isolation between the transmitter and receiver of a full-duplex device using a single antenna for transmission and reception.

{% assign f = "/fig/circulator_type_06.svg" %}
{% assign c = "A circulator can be used to establish isolation between a transmitter and receiver sharing an antenna. Leakage through the circulator and reflections off the environment give rise to self-interference, however." %}
{% include image.html file=f caption=c width=70 link=false %}

One may reason that with an ideal circulator and with two radios operating in free space, full-duplex operation is trivial since perfect isolation is achieved between a radio's transmit signal and a desired receive signal. 
In reality, a circulator effectively offers limited RF isolation between its ports, which introduces self-interference at the receiver. 
This is due to a number of factors, most notably the leakage between ports, reflections caused by imperfect matching at the antenna, and reflections off the environment.
Circulators with small form-factors that offer high isolation for full-duplex are an active area of research with immense potential.
Nonetheless, analog and digital self-interference cancellation can be used in conjunction with a circulator for single-antenna full-duplex transceivers.
In such cases, self-interference cancellation aims to cancel circulator leakage, as well as reflections off the environment and from the antenna.

### Additional Readings

Here are a few good papers to read more on self-interference cancellation:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf)
- [In-Band Full-Duplex Wireless: Challenges and Opportunities](https://ieeexplore.ieee.org/document/6832464)
- [Full-Duplex Transceiver System Calculations: Analysis of ADC and Linearity Challenges](https://ieeexplore.ieee.org/document/6782415)
- [Full-Duplex Mobile Device: Pushing the Limits](https://ieeexplore.ieee.org/document/7565192)
- [Widely Linear Digital Self-Interference Cancellation in Direct-Conversion Full-Duplex Transceiver](https://ieeexplore.ieee.org/document/6832439)

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
