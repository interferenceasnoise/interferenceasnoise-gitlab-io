---
layout: note
title: "Fundamentals of Antenna Arrays"
categories: [wireless,mmwave]
date: 2023-03-27
summary: "An introduction to antenna array theory and beamforming."
summary-extended: >
  Multiple antennas are used in a variety of modern applications including wireless communication, radar, and radio astronomy, among others. This note overviews the fundamentals of antenna array signal processing and how antenna arrays can use beamforming to focus energy in a particular direction.
published: true
---


### What is an Antenna? 

A wireless device---or transceiver---is equipped with one or more antennas to wireless communicate using electromagnetic waves.
An antenna can be thought of as converting electrical signals to electromagnetic waves, as illustrated below.
Antennas are often passive pieces of hardwawre (think of just a piece of metal), meaning they don't require any input power.
Rather, their physical properties allow them to resonate when they are fed (input) with electrical signals at just the right frequency.
In other words, this **resonant frequency** depends on the physical characteristics of the antenna (e.g., its size).
For example, larger antennas resonate at lower frequencies, in general.

{% assign f = "/fig/wireless_radio_type_11.svg" %}
{% assign c = "When fed with electrical current, an antenna radiates electromagnetic waves." %}
{% include image.html file=f caption=c width=50 link=false %}

In addition to radiating electromagnetic waves, antennas can also be used for receiving such.
In other words, antennas can be used for both transmission and reception of wireless signals, and in fact, antennas exhibit **transmit-receive reciprocity**, which means that many of their properties (such as antenna gain) are the same whether the antenna is used for both transmission or reception.
When an electromagnetic wave strikes an antenna, the antenna will generate an electrical current, assuming the electromagnetic wave is at---or close to---the resonant frequency of the antenna.

{% assign f = "/fig/wireless_radio_type_15.svg" %}
{% assign c = "When electromagnetic waves strike an antenna, it generates an electrical current." %}
{% include image.html file=f caption=c width=50 link=false %}

Antenna theory is a rich subject of its own, and this note naturally cannot cover all the details of how antennas work. 
As such, we direct interested readers to Constantine Balanis's textbook _Antenna Theory: Analysis and Design_ for comprehensive coverage of the subject.


### Electromagnetic Waves

As was done with antennas, we now overview necessary background on electromagnetic waves, with the understanding that more details can be found elsewhere. 
An electromagnetic wave radiated (or received) by an antenna can be expressed as a sinusoid $y(t)$ propagating at some **frequency** $f$, as illustrated below.

{% assign f = "/fig/signals_type_09.svg" %}
{% assign c = "A wave propagates as a function of time, completing $f$ cycles per second." %}
{% include image.html file=f caption=c width=75 link=false %}

For every second that passes in time, the wave completes $f$ cycles; hence, $f$ has units of cycles/second, more commonly referred to as Hertz (Hz).
Inverting the frequency $f$ of the wave, we obtain its **period** $1/f$, which is the time consumed when completing one cycle and has units of seconds per cycle.

Electromagnetic waves can be thought of as functions of time as described thus far, but they can also be thought of equivalently as functions of space, since as time passes, the signal propagates across space. 
As illustrated below, when rewriting the time axis as a spatial axis, it becomes clear that the **wavelength** $\lambda$ is the distance traveled when completing one cycle. 
The period and the wavelength can therefore be thought of as analogous quantities in the two time and space domains, respectively.

{% assign f = "/fig/signals_type_05.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=75 link=false %}

Electromagnetic waves propagate at the **speed of light**, $\mathrm{c} \approx 3 \times 10^8$ meters/sec. (On Earth, they actually travel a little slower.) 
Breaking down the units of wavelength $\lambda$ (meters per cycle) and frequency $f$ (cycles per second), we obtain the following relationship. 

$$
\underbrace{\mathrm{c}}_{\textsf{meters/sec}} = \underbrace{\lambda}_{\textsf{meters/cycle}} \cdot \underbrace{f}_{\textsf{cycles/sec}}
$$

From this equation, along with our discussions prior, it becomes clear that wavelength shrinks as frequency increases.

{% assign f = "/fig/signals_type_12.svg" %}
{% assign c = "Doubling the frequency of a wave cuts its wavelength in half." %}
{% include image.html file=f caption=c width=75 link=false %}

### Plane Wave Assumption

An idealized antenna can be thought of as one which is infinitesimally small and radiates equally in all directions, whose energy radiates outward spherically.
This concept of a so-called **isotropic antenna** does not exist in the real world but is nonetheless a useful tool in analyzing antennas.
Consider a transmitter radiating energy with an isotropic antenna, as illustrated in the animated figure below.

{% assign f = "/fig/isotropic.gif" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=50 link=false %}

Electromagnetic waves radiate outward from the antenna in a spherical fashion, depicted as circles here in this 2-D illustration.
This can be thought as similar to dropping a pebble in a still pond; ripples propagate outward in all directions from where the pebble strikes the water.

Suppose there are two points separated from one another by some small distance $D$.
Referencing the figure below, consider the case when the two points are close to the transmitter.
As an electromagnetic wave propagates out spherically from the transmitter, a **spherical wavefront** strikes the two points.
In other words, the wave propagating over the intersection of these points appears spherical.

Slightly further from the transmitter, these two points see a slightly less spherical wavefront.
Given their fixed separation $D$, the further from the transmitter, the less spherical the wavefront appears from their perspective.
Far from the transmitter, the wave striking these two points appears nearly planar, must like how the Earth appears flat on its surface due to its large radius.

{% assign f = "/fig/array_theory_type_43.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=50 link=false %}

The distance at which the spherical wavefront begins to appear as a planar wavefront is called the **Rayleigh distance**, often also called the **Fraunhofer distance** or the **far-field distance/boundary**.

From the perspective an antenna whose largest dimension is $D$, the Rayleigh distance can be written as

$$\mathsf{Rayleigh~distance} = 2 D^2 / \lambda$$

where $\lambda$ is the wavelength, as usual.
Put simply, waves radiated by an isotropic antenna impinging a non-isotropic antenna with largest dimension $D$ appear approximately planar.
Thanks to transmit-receive reciprocity of antennas, the Rayleigh distance also corresponds to the distance beyond which the transmitted radiation pattern of a non-isotropic antenna are no longer a function of distance; this concept of far-field in the context of transmission involves greater explanation involving electromagnetic theory.
It is important to note that coupling can manfiest with an antenna when extremely close to it. 

### An Array of Two Antennas in Far-Field

Consider two antennas close to one another, separated by some distance $d$, as illustrated below.
Suppose a distant isotropic antenna---located in the far-field---radiates energy, which then impinges these two antennas.
Under these far-field conditions, the radiated energy strikes the two antennas as a plane wave.

{% assign f = "/fig/plane_01.gif" %}
{% assign c = "A plane wave impinges two antennas simultaneously." %}
{% include image.html file=f caption=c width=70 link=false %}

When the alignment of these two antennas is perpendicular to the plane wave's direction of propagation, the plane wave strikes the two antennas simultaneously.

{% assign f = "/fig/plane_02.gif" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=70 link=false %}

{% assign f = "/fig/signals_type_06.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=75 link=false %}

{% assign f = "/fig/array_theory_type_10.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=60 link=false %}


### A Linear Array of $N$ Antennas

{% assign f = "/fig/array_theory_type_39.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=60 link=false %}

{% assign f = "/fig/array_theory_type_41.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=80 link=false %}


### An Arbitrary Array of $N$ Antennas

{% assign f = "/fig/main_example_01_array_2d_8.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}

{% assign f1 = "/fig/main_example_03_array.svg" %}
{% assign f2 = "/fig/main_example_02_array.svg" %}
{% assign c = "Test." %}
{% include image-double.html file1=f1 file2=f2 width1=48 width2=48 caption=c link=false %}

{% assign f = "/fig/experiments_att_geometry_type_02.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}


### Beamforming

{% assign f = "/fig/array_beamforming_type_01.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=80 link=false %}

{% assign f = "/fig/beam_8_0.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}

{% assign f = "/fig/polar_8_0.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}

{% assign f = "/fig/polar_8_30.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}



### Antenna Arrays in the Real World

#### Wi-Fi

{% assign f1 = "/fig/router_01.jpg" %}
{% assign f2 = "/fig/router_02.jpg" %}
{% assign c = "The Very Large Array is a reconfigurable array of radio telescopes observing signals from deep space." %}
{% include image-double.html file1=f1 file2=f2 width1=45 width2=40 caption=c link=false %}

#### Radar 

{% assign f1 = "/fig/ti_radar.jpg" %}
{% assign f2 = "/fig/huge_radar_array.jpg" %}
{% assign c = "The Very Large Array is a reconfigurable array of radio telescopes observing signals from deep space." %}
{% include image-double.html file1=f1 file2=f2 width1=36.5 width2=57 caption=c link=false %}

#### 5G Cellular Communications

{% assign f1 = "/fig/anokiwave_v01.png" %}
{% assign f2 = "/fig/array_16_by_16_28_GHz_type_02.svg" %}
{% assign c = "The Very Large Array is a reconfigurable array of radio telescopes observing signals from deep space." %}
{% include image-double.html file1=f1 file2=f2 width1=55 width2=40 caption=c link=false %}

#### Radio Astronomy

{% assign f1 = "/fig/vla_04.jpg" %}
{% assign f2 = "/fig/vla_aerial.jpg" %}
{% assign c = "The Very Large Array is a reconfigurable array of radio telescopes observing signals from deep space." %}
{% include image-double.html file1=f1 file2=f2 width1=45 width2=51 caption=c link=false %}

### Extra

{% assign f = "/fig/main_example_03_array.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}

{% assign f = "/fig/main_example_02_array.svg" %}
{% assign c = "Caption." %}
{% include image.html file=f caption=c width=60 link=false %}


### Additional Readings

Here are a few good references on antenna arrays:
- Test

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
