---
layout: note
title: "What is Joint Communication and Sensing?"
categories: [wireless]
date: 2023-03-15
summary: An introduction to joint communication and sensing.
summary-extended: An extended summary.
published: true
---

### Test

Until now, wireless communication systems have almost exclusively operated in a **half-duplex** fashion, where transmission and reception of radio waves have typically been separated---or orthogonalized---in the time domain or frequency domain. Put simply, signals transmitted or received by a traditional half-duplex system exist in different frequency bands or at different times, referred to as frequency-division duplexing (FDD) and time-division duplexing (TDD), respectively. 

{% assign f1 = "/fig/resource_tdd.svg" %}
{% assign f2 = "/fig/resource_fdd.svg" %}
{% assign c = "(left) TDD divides radio resources in time. (right) FDD divides radio resources in frequency." %}
{% include image-double.html file1=f1 file2=f2 width1=35 width2=35 caption=c link=false %}

{% assign f = "/fig/fdx_si.svg" %}
{% assign c = "A device attempting to simultaneously transmit and receive over the same frequency spectrum incurs self-interference." %}
{% include image.html file=f caption=c width=75 link=false %}


### Additional Readings

Here are a few good papers to read more on full-duplex:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf)
- [In-Band Full-Duplex Wireless: Challenges and Opportunities](https://ieeexplore.ieee.org/document/6832464)
- [Full-Duplex Transceiver System Calculations: Analysis of ADC and Linearity Challenges](https://ieeexplore.ieee.org/document/6782415)
- [Full-Duplex Mobile Device: Pushing the Limits](https://ieeexplore.ieee.org/document/7565192)
- [Widely Linear Digital Self-Interference Cancellation in Direct-Conversion Full-Duplex Transceiver](https://ieeexplore.ieee.org/document/6832439)
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf)

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
