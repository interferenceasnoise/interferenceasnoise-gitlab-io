---
layout: note
title:  "Code Snippet: Subfigures in LaTeX"
date: 2020-06-01
categories: [latex]
summary: A code snippet for subfigures in LaTeX.
---


{% highlight latex %}
\usepackage[caption=false,font=footnotesize]{subfig}
{% endhighlight %}

{% highlight latex %}
\begin{figure}
   \centering
   \subfloat[Caption a.]{\includegraphics[width=0.48\linewidth]{}
   \label{fig:subfig-a}}
   \quad
   \subfloat[Caption b.]{\includegraphics[width=0.48\linewidth]{}
   \label{fig:subfig-b}}
  \caption{Caption here.}
  \label{fig:subfigs}
\end{figure}
{% endhighlight %}



