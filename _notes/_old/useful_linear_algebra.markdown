---
layout: note
title:  "Useful Linear Algebra"
categories: [math]
date: 2020-11-19
summary: A growing list of useful concepts and tools from linear algebra.
published: false
---


### Notation

- I use lowercase $$x$$ to represent scalars and vectors.
- I use uppercase $$X$$ to represent scalars and matrices.
- Unless otherwise stated, let $$x$$ be $$N \times 1$$ and complex.
- Unless otherwise stated, let $$X$$ be $$M \times N$$ and complex.
- $$\sigma_{i}(A)$$ is the $$i$$-th largest singular value of $$A$$.
- $$\lambda_{i}(A)$$ is the $$i$$-th largest eigenvalue of $$A$$.
- $$\mathrm{vec}(A)$$ is the column vectorization of $$A$$.

### Vector Norms

$$
\begin{align}
||x||^2_{2} = x' x = \mathrm{tr}(xx')
\end{align}
$$

$$
\begin{align}
||Ax||^2_{2} = (Ax)^{'} (Ax) = \mathrm{tr}(A x x' A')
\end{align}
$$

### Matrix Norms

$$
||A||_2 = \sigma_{1}(A)
$$


$$
||A||_{\mathrm{F}}^2 = \sum_{i} \sigma^2_{i}(A)
$$

$$
||A||_{\mathrm{F}} = ||\mathrm{vec}(A)||_{2}
$$

$$
||A||_{2} \leq ||A||_{\mathrm{F}}
$$


$$
||AB||_{\mathrm{F}} \leq ||A||_{\mathrm{F}} \cdot
||B||_{\mathrm{F}}
$$


$$
||AB||_{\mathrm{F}} \leq ||A||_{2} \cdot
||B||_{\mathrm{F}}
$$

### Trace


(Cyclic Property) For properly sized matrices, 

$$
\begin{align}
\mathrm{tr}(A B C) = \mathrm{tr}(C A B) = \mathrm{tr}(B C A)
\end{align}
$$


