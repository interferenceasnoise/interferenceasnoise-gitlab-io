---
layout: note
title:  "A Simple System Model for mmWave Full-Duplex"
categories: [fdx]
tags: fdx
date: 2021-06-27
summary: Outlining key components of a simple mmWave full-duplex system.
published: false
---

*This post is part of [A Series on Millimeter Wave Full-Duplex](/notes/mmwave-full-duplex-series).*

If you are new to millimeter wave communication, please read [Millimeter Wave Communication Systems](/notes/mmwave-full-duplex-mmwave).



