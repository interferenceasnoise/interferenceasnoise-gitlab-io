---
layout: note
title:  "AD9361 Setup and Interfacing"
categories: [wireless]
date: 2020-03-15
summary: This guide will describe how I setup and use the Analog Devices AD9361 software-defined radio platform. I interface with the radio using Python via Analog Device's library libiio.
---

### libiio

Analog Devices publishes a library called `libiio` to connect to various platforms, including the AD9361. We will need to install it. 

Please follow [their instructions](https://wiki.analog.com/resources/tools-software/linux-software/libiio) on how to install `libiio`. It should be fairly straightforward.

There is one thing issue that I have had with installing the Python support. To fix this, do the following.

Navigate to the `libiio/` directory that you cloned during installation.

Open `libiio/bindings/python` in the file explorer. There should be a file called `setup.py.cmakein` or `setup.py`.

If `setup.py.cmakein` is there but not `setup.py`, create a copy of `setup.py.cmakein` and rename it `setup.py`.

In a terminal at this location, run `sudo python setup.py install` (or `sudo python3 setup.py install`). This allows us to access libiio from Python.


### Hardware

This will guide you through setting up the AD9361 using the following hardware:
 - ADRV9361-Z7035 software-defined radio
 - ADRV1CRR-BOB break-out board


Attach the ADRV9361-Z7035 to the ADRV1CRR-BOB. Be sure to push down with some force to make sure it connects firmly.

The ADRV9361-Z7035 should have come with an SD card. Download these two files and copy them to the main directory of the SD card.
 - [devicetree.dtb](/pdf/ad9361/devicetree.dtb)
 - [BOOT.BIN](pdf/ad9361/BOOT.BIN)

Plug the SD card into the ADRV9361-Z7035.

Connect the micro-USB and Ethernet to your host PC. The provided micro-USB did not want to plug into the board very easily.

Connect the DC power supply to the board.

Turn on the big red switch (S5) to power on the board.


### Network Setup

With the board connected to your host and powered on, we will now configure the Ethernet connection. 

Give the board about a minute to fully boot up before proceeding.


#### Linux

Open a terminal.

Type `sudo screen /dev/ttyUSB0 115200` to remote into the device. (Install `screen` if you need to.)

Type `stop network-manager`. This fixes some connection issues I had with my board.

Type `ifconfig eth0 192.168.3.2 up`. This sets the IP address of the board to `192.168.3.2`.

I usually leave this terminal open when working with the device.

Open a new terminal.

Type `ifconfig` to see your Ethernet devices (e.g., `eth0`, `eth1`). Choose the one connected to the board. This may take some trial and error. A USB-to-Ethernet dongle is convenient and can be easily identified by unplugging then plugging it in.

Type `sudo ifconfig <Ethernet-device> 192.168.3.5 up` where <Ethernet-device> is the Ethernet device of your host. This sets the IP address of your host to `192.168.3.5`.

Run `ping 192.168.3.2` to ensure the connection between the devices is successful.

If you have issues with network stability on your host machine, this may help you. In, `/etc/NetworkManager/NetworkManager.conf`, change `managed=false` to `managed=true`. Then, run `sudo service network-manager restart`.


### Python Interfacing

With the Ethernet connection set up, we can now connect to the device via Python.

To confirm the libiio Python bindings were successfully installed, open a terminal and run `python` (or `python3`).

Type `import iio`. 

If there is an error message, the Python bindings weren't installed correctly. (Be sure to use `python` or `python3` consistently.)

If there is no error message, that's good. That means we can try to interface with the device via Python.


#### Install pyadi-iio

Analog Devices publishes a convenient Python library for interfacing with the AD9361 (among other devices) called pyadi-iio.

Install pyadi-iio using the [Analog Devices instructions](https://wiki.analog.com/resources/tools-software/linux-software/pyadi-iio) or via `sudo pip install pyadi-iio` (or `sudo pip3 install pyadi-iio`).


#### Your First Python Script

If everything is successful so far, we are now ready to actually interface with the board.

In your Python editor of choice, create a script as follows and run it.

{% highlight python %}
# Import pyadi-iio library
import adi

# Create device and connect
IP_ADDRESS_AD9361 = '192.168.3.2'
device_uri = "ip:" + IP_ADDRESS_AD9361
sdr = adi.ad9361(uri=device_uri)

# Set receiver LO
sdr.rx_lo = 2500000000 # 2.5 GHz

# Get receiver LO value
print("RX LO %s" % (sdr.rx_lo))
{% endhighlight %}

Hopefully, everything worked for you. 

More information on pyadi-iio can be found at [this website](https://analogdevicesinc.github.io/pyadi-iio/). 

I will also be posting [more notes](/notes/) on this topic.
