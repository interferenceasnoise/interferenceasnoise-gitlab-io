---
layout: note
title:  "Topics in Wireless and Signal Processing"
categories: [other]
date: 2020-01-15
summary: A growing list of topics that undergraduate and early graduate students in signal processing/wireless may want to look into. It's hard to know what you don't know.
---

Many of the topics I list here have good references on my [Resources](/resources/) page.

### Wireless

- Low pass, band pass, and high pass filters
- Upconversion and downconversion
- Pulse shaping
- Matched filter
- Additive white Gaussian noise
- Multipath channels
- Rayleigh fading
- Rician fading
- Signal-to-noise-ratio
- Signal-to-interference-plus-noise ratio
- Link budget
- Free-space path loss
- Channel estimation (least mean squares, least squares, MMSE)
- Equalization (zero-forcing, MMSE)
- Multiple-input multiple-output (MIMO) communication
- Millimeter-wave communication
- Hybrid beamforming for millimeter-wave communication
- Saleh-Valenzuela channel model for millimeter-wave communication
- Compressed sensing based channel estimation at millimeter-wave
- Spherical-wave MIMO channel model
- Power amplifiers
- Low noise amplifiers
- Unlicensed spectrum
- Medium access control (MAC)
- In-band full-duplex

> Recommended book on MIMO communication: [Foundations of MIMO Communication](https://www.cambridge.org/core/books/foundations-of-mimo-communication/D1D999D61E48C62C44240EF2341A29A1) by Robert Heath and Angel Lozano.

> Recommended overview of mmWave communication: [An Overview of Signal Processing Techniques for Millimeter Wave MIMO Systems](https://arxiv.org/pdf/1512.03007.pdf) by Robert Heath, et al.


### Linear Algebra

- Least squares
- Singular value decomposition
- QR decomposition
- Gram Schmidt algorithm
- Eigenvalues and eigenvectors
- Eigendecomposition
- Rayleigh quotient
- Vector norms
- Induced matrix norms
- Frobenius norm
- Positive semidefinite matrices
- Matrix calculus
- Projections
- Trace
- Determinant
- Norm inequalities

> Recommended introductory textbook: [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/) by Gilbert Strang.


### Array Signal Processing

- Uniform linear array
- Uniform planar array
- Conjugate beamforming (matched filter)
- Zero-forcing beamforming
- Minimum mean square error beamforming
- Beamforming codebooks
- Robust beamforming


### Algorithms, Estimation, and Optimization

- Least mean squares (LMS) estimation
- Least squares estimation
- Linear programming
- Convex optimization
- Robust optimization
