---
layout: note
title: "What is Beam Alignment?"
categories: [wireless]
date: 2023-03-15
summary: An introduction to beam alignment in mmWave communication systems.
published: true
---

Beam alignment (also called "beam management" or "beam training") is a critical component to real-world mmWave communication systems, like those in 5G and IEEE 802.11ay (60 GHz Wi-Fi).

Here are a few good papers to read more on beam alignment:
- Paper 1
- Paper 2
- Paper 3
