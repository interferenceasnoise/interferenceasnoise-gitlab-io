---
layout: note
title:  "My LaTeX Starters"
categories: [latex]
date: 2020-01-20
summary: In this note, I provide some of my starter files for LaTeX-based documents. This will be a growing list over time.
---

Download the zipped folder containing the starter files (i.e., templates, boilerplates, etc.) or view an example of document by clicking its PDF link.

- IEEE Conference Paper (IEEEtran), [[zip](/pdf/latexstarters/starter_ieee_conf.zip)], [[pdf](/pdf/latexstarters/starter_ieee_conf.pdf)]
- Simple Beamer Slides, [[zip](/pdf/latexstarters/starter_beamer_simple.zip)], [[pdf](/pdf/latexstarters/starter_beamer_simple.pdf)]

Especially for IEEE papers, please refer to your specific formatting requirements. I cannot guarantee that mine will suffice.

My IEEE Conference Paper is specifically tailored for easier uploading to arXiv. Again, however, I cannot guarantee that you will not run into trouble when doing so.

My Simple Beamer Slides were inspired by the slides used by [Stephen Boyd](http://web.stanford.edu/~boyd/) and [this StackExchange post](https://tex.stackexchange.com/questions/107569/what-beamer-theme-is-used-in-boyds-convex-optimization-slides).
