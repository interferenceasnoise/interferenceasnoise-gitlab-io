---
layout: note
title:  "Useful Math"
categories: [other]
published: false
---

> Some useful math I thought was worth jotting down. Maybe you will find it useful too.


### Linear Algebra

$$
\begin{align}
||x||^2_{2} = x^{*} x = \mathrm{tr}(xx^{*})
\end{align}
$$


### Matrix Calculus

Taking the derivative of matrix/vector equations with respect to scalars, vectors, and matrices.

$$
\begin{align}
\frac{d}{dX} \mathrm{tr}(X) &= I
\end{align}
$$