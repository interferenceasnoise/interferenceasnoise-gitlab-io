---
layout: note
title:  "What is full-duplex?"
categories: [wireless]
date: 2020-03-18
published: false
summary: In this note, I provide a high-level description of full-duplex. You have probably seen this term used frequently throughout this site, so I figured I would go ahead and describe it using a simple example.
---

### In layman's terms

Imagine Bob is standing at one end of a football field. Alice is standing at the other end, separated by about 100 meters. If Alice yells something to Bob, he will hear it somewhat faintly, since her voice has traveled quite a distance to reach Bob. On the other hand, when Bob yells to Alice, his voice will sound faint at Alice. 

If Bob tries to yell to Alice while Alice yells to Bob, Bob won't be able to understand what Alice is saying because his yelling is much louder than Alice's voice at Bob. Of course, this means they cannot talk at the same time.

The two take turn yelling to each other. First Alice yells. Then Bob. Then Alice, and so on. This is analogous to half-duplex communication.

Full-duplex communication is analogous to Alice and Bob being able to yell to each other simultaneously, while still understanding what the other is saying. This is done by cancelling one's own yelling completely so that it doesn't drown out the other's. 

Since both can talk at the same time, it takes half the time for them to say what they need to say. This means communication is twice as fast.

How is this done? The method, in full-duplex terminology, is self-interference cancellation (SIC).


### A little more detail

How exactly is SIC achieved? Well, it relies on the fact that a transceiver (Bob or Alice) knows what he/she yelled to the other.

Knowing what we yelled, how about we just subtract it from what we heard? If we can do that, Bob will hear Alice's faint voice even while he is yelling. Her voice won't get drowned out by his own yelling, from his perspective.

The SIC process is as follows. Take Bob's transmitted signal (the yelled phrase), scale it, and invert it. Then take the inverted signal and add it to what Bob hears. This scaled, inverted copy of what was said will cancel (destructively add) to what was heard, leaving only Alice's voice. Alice's voice will still be as weak as it was when it arrived at Bob's ears, but at least Bob's yelling won't have made it impossible to hear what Alice has said.


