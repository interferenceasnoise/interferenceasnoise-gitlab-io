---
layout: note
title:  "Slides: LaTeX Tips, Tricks, and Good Practice"
date: 2021-11-25
categories: [latex]
summary: LaTeX practice that I have found to work well for me.
---

Slides are available [here](/pdf/latex_tips.pdf).



