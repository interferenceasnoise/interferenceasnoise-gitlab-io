---
layout: note
title:  "Using Git with Overleaf"
categories: [latex]
date: 2020-02-01
summary: How to use Git on your Overleaf projects.
---

[Overleaf](https://www.overleaf.com/) is a useful tool for collaborating on LaTeX based documents. The workflow I use, however, when working alone on documents is to prepare them locally using TeXstudio and an installation of TeX Live. I prefer this over Overleaf because it's the system I am used to and can work much more quickly. I like TeXstudio's custom macros, compiliation time is much quicker, and I like to zoom in to inspect the PDF throughout its preparation.

When I first needed to collaborate on a paper, I knew Overleaf was the practical route to take since I realize my method wasn't the most ideal for others to adapt to. I was happy to discover that Overleaf has Git support!

Here's how to set it up.

1. Create your Overleaf project as usual or open an existing one.
2. In the top left of your Overleaf project, click `Menu`.
3. Under `Sync`, click `Git`.
4. Copy the entire Git clone command.
5. Open a terminal (or Git Bash) in the directory you want to house the project.
6. Clone the project using the Git clone command copied before.
7. When prompted, enter your Overleaf username and password.
8. Make changes as desired. When, ready to push your changes do the following.
9. `git add .`
10. `git commit -m 'message'`
11. `git push`
12. Enter your Overleaf username and password.

That's it! Your collaborators can see the project files in the browser at Overleaf or they can also use Git if desired.
