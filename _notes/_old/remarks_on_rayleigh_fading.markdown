---
layout: note
title:  "Brief Summarizing Remarks on Rayleigh Fading"
categories: [wireless]
date: 2021-03-03
summary: A quick summary of the distributions associated with Rayleigh fading.
published: true
---


A Rayleigh-faded channel $$h$$ is often written as




$$ h \sim \mathcal{N}_{\mathbb{C}}(0,1) $$




which is short-hand for saying that 




$$ \Re\{h\} \sim \mathcal{N}(0,1/2) \\ \Im\{h\} \sim \mathcal{N}(0,1/2) $$




where $$\mathcal{N}(\mu,\sigma^2)$$ is a normal distribution with mean $$\mu$$ and variance $$\sigma^2$$.




We have $$\sigma^2 = 1/2$$ in the case of $$h \sim \mathcal{N}_{\mathbb{C}}(0,1)$$.




The magnitude of the channel $$\lvert h \rvert$$ follows a Rayleigh distribution where $$\sigma = 1/\sqrt{2}$$ whose PDF is




$$ f(x) = \frac{x}{\sigma^2} \exp \left( \frac{-x^2}{2\sigma^2} \right) $$




or, taking $$\Omega = 2 \sigma^2$$, is equivalent to




$$ f(x) = \frac{2x}{\Omega} \exp \left( \frac{-x^2}{\Omega} \right) $$




Note that when $$\sigma^2 = 1/2$$, $$\Omega = 1$$.




Sometimes the Rayleigh distribution is parameterized by $$\sigma$$. Other times it is parameterized by $$\Omega = 2 \sigma^2$$.




The distribution of $$\lvert h \rvert^2$$ follows an exponential distribution with PDF




$$ f(x) = \lambda \cdot \exp \left( -\lambda x \right) $$




where $$\lambda = \frac{1}{2\sigma^2} = 1$$ (when $$\sigma^2 = 1/2$$) and whose mean is $$\lambda$$, implying that $$\mathbb{E}\left[ \lvert h \rvert^2 \right] = 1$$.




In other words, the average channel power is 1. This is convenient for abstracting the large-scale path loss (e.g., Friis) and large-scale fading (e.g., log-normal shadowing) from the small-scale fading distribution (e.g., Rayleigh fading).




One could also define use $$\Omega = 2 \sigma^2 \implies \lambda = \frac{1}{\Omega}$$ to write the PDF as




$$ f(x) = \frac{1}{\Omega} \cdot \exp \left( \frac{-x}{\Omega} \right) $$




Note that $$h$$ can also be written as having distribution




$$ h \sim \mathcal{N}_{\mathbb{C}}(0,\Omega) $$



where now it's clear that $$\Omega$$ is the average power of $$h$$ (i.e., its variance is its average power since $$h$$ is zero mean).

