---
layout: note
title:  "A Simple Compressed Sensing Example"
date: 2019-05-01
categories: [wireless]
summary: In this note, I will provide a simple MATLAB script demonstrating compressed sensing.
---

### Introduction

The idea behind compressed sensing (CS) is that a sparse signal can be sampled a fewer number of times than had it not been sparse.

Of course, by the Shannon-Nyquist sampling theorem, we know that a continuous, band-limited time-domain signal can be perfectly reconstructed by a discrete samples, provided it is sampled at a rate at least twice its highest frequency component. 

If there were ways to reconstruct a signal with fewer samples, this would be beneficial for reasons that vary with application.

For CS to apply, the signal that wants to be observed must be sparse in some basis. For example, our true signal $$\mathbf{y}$$ is sparse in the basis $$\mathbf{A}$$ when the number of nonzero (or close to nonzero) elements of $$\mathbf{x}$$ is small relative to its size. 

$$
\mathbf{y} = \mathbf{A} \mathbf{x}
$$

A good analogy for being sparse in one domain but not another is the duality between the time- and frequency-domains of a signal. A signal, for example, may be sparse in time, consisting of a few impulse (Dirac delta) functions, but as we know, this is wideband in frequency where it is not sparse.

### The Approach

The approach of CS is to measure (or sense) a signal using sensing vectors that are not correlated with the basis which the signal is sparse.

Thus, the sensing output $$\mathbf{g}$$ will be

$$
\begin{align}
\mathbf{g} &= \mathbf{B}^{\textsf{H}} \mathbf{y} \\
           &= \mathbf{B}^{\textsf{H}} \mathbf{A} \mathbf{x}
\end{align}
$$

where $$\mathbf{B}$$ is our sensing matrix.

The columns of $$\mathbf{B}$$ are sensing vectors. Ideally, these should be uncorrelated with $$\mathbf{A}$$. Thus, constructing $$\mathbf{B}$$ from random vectors works well.

From here, we have our observations $$\mathbf{g}$$. The goal is to reconstruct $$\mathbf{y}$$ accurately from a small number of observations in $$\mathbf{g}$$.

Suppose we know the domain in which our true signal $$\mathbf{y}$$ is sparse. Thus, we know the basis matrix $$\mathbf{A}$$. We also know our sensing matrix, of course, since that is what we used to make the observations $$\mathbf{g}$$. 

This sets the stage for solving for $$\mathbf{x}^\star$$ where 

$$
\mathbf{g} = \mathbf{B}^{\textsf{H}} \mathbf{A} \mathbf{x}^\star
$$

We have more unknowns than we do equations, so this is an underdetermined system. Being sparse, however, we do know something about $$\mathbf{x}^\star$$ -- it must have a small number of nonzero entries. This motivates us to minimize the $$\ell_0$$ norm (the number of nonzero entries). 

That is hard to solve, though, since the $$\ell_0$$ norm is not differentiable (so gradient methods cannot be used). Therefore, we transform this into minimizing the $$\ell_1$$ norm instead. 

$$
\min_{\mathbf{x}^{\star}} \left \| \mathbf{x}^\star \right \|_1 \ \text{s.t.} \ \mathbf{g} = \mathbf{B}^{\textsf{H}} \mathbf{A} \mathbf{x}^\star
$$

Solving this optimization problem results in finding the sparse coefficient vector that can then be used to create the estimated signal using our basis $$\mathbf{A}$$.

$$
\hat{\mathbf{y}} = \mathbf{A} \mathbf{x}^\star
$$

### Example: Fourier Series

In this example, I create a signal composed of 6 sinusoids (approaching a square wave) meaning that it is sparse in the frequency domain. Each of the sinusoids and the resulting sum of them is 128 samples long.

This signal is unknown to a device trying to sense the channel. Thus, let's say a device wanted to sense the channel to determine what frequencies the signal occupied. While it may be able to assume that the signal lies somewhere between DC and half the sampling frequency, it would not know what frequency components make up the signal.

Following Shannon-Nyquist, measuring the signal would require sampling the signal at the sampling rate for an appropriate number of samples such that the desired frequency resolution is met. In this case, my basis of 128 sinusoids are spaced from DC to half the sampling rate. To estimate the signal's frequency components, we would use an FFT or DFT matrix, which correlates 128 sinusoids at varying frequencies with the time domain signal. Unknown of the frequency components beforehand, we must perform a full 128-point DFT. 

Using CS, however, we can perform only 32 correlations (measurements) and accurately identify the frequency components made up by the true signal. Note, that this measurement requires measurement vectors. In this CS example, the measurement vectors are random. In the DFT, the measurement vectors are sinusoids.

After taking the 32 measurements, we can use $$\ell_1$$ minimization to solve for the estimated sparse coefficients and then reconstruct an estimate of the true signal.

Note, we use [l1 Magic](https://statweb.stanford.edu/~candes/software/l1magic/) for the `l1eq_pd()` function which performs $$\ell_1$$ minimization.

![Estimated coefficients versus true coefficients.](/images/cs/cs_coefficients.svg#center, "Estimated coefficients versus true coefficients.")

![Reconstructed signal versus true signal.](/images/cs/cs_reconstructed.svg#center, "Reconstructed signal versus true signal.")

{% highlight matlab %}
clearvars; clc;

% Setup
N = 128; % length of basis
n = (0:N-1)';
K = 128; % number of basis functions

% Basis (A == Psi)
A = [];
for i = 1:K
    s = sin(2*pi*i*n/N/2); % basis of (unique) sinusoids
    % s = s ./ norm(s); % to be unit norm
    A = [A s];
end

% Sparse coefficients
idx_sparse = [1, 3, 5, 7, 9, 11]; % indeces having nonzero sparsity
x = zeros(K,1);
x(idx_sparse) = 4/pi * 1./idx_sparse; % true sparse coefficients (square wave)

% True signal (y == f)
y = A * x;
figure(1);
plot(y,'-k'); grid on;
legend('true');

% Measurements (B == Phi)
M = 32; % number of random measurements
B = randn(N,M); % measurement/sensing matrix (random)
c = (B.' * y); % sense the actual signal with our sensing matrix

% Solve
Aeq = B.' * A; % Constraint: c == B.' * y == B.' * A * x
beq = c; % Aeq * x == beq
x0 = ones(K,1); % initial guess
g = l1eq_pd(x0,Aeq,[],beq); % estimated sparse coefficients

% Compare to original sparse coefficients
figure(2);
plot(x,'ko'); grid on; hold on;
plot(g,'rx'); hold off;
legend('true','estimated');

% Reconstruction
figure(3);
plot(y,'-k'); grid on; hold on;
plot(A * g,'--r'); hold off;
legend('true','estimated');
{% endhighlight %}



