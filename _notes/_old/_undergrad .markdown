---
layout: note
title:  "My Undergrad Experience"
categories: [other]
---

> An overview of the LaTeX practice that I have found to be productive.


### 
{% highlight matlab %}

{% endhighlight %}

To get MATLAB to render LaTeX math and text, I put the following in my `startup.m` file. You could also copy this and run in the MATLAB Command Window.

![Comparing the number of RF chains in BFC.](/images/mmwave/comparing_nrf.svg#center, "Comparing the number of RF chains in BFC.")
