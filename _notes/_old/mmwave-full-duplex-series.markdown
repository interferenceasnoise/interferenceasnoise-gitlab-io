---
layout: note
title:  "A Series on Millimeter Wave Full-Duplex"
categories: [wireless]
date: 2021-06-26
summary: A collection of posts providing an overview of mmWave full-duplex.
published: false
---


In this series of notes, I provide an introduction and overview of millimeter wave (mmWave) full-duplex.
The goal of this series is to provide researchers, engineers, and those otherwise interested with a centralized walk-through of mmWave full-duplex without needing to navigate and wade through overwhelming amounts of literature.
I will highlight the common assumptions, proposed solutions, measurements, and key challenges and unknowns regarding of mmWave full-duplex.
In addition to these posts, I will recommend research papers that are particularly relevant to certain topics.

The collection of posts are as follows, which I recommend to be read in the order listed.

{% assign notes = site.notes | where:"tags","fdx" %}
{% for note in notes %}
- [**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
{% endfor %}

