---
layout: note
title: "Glossary of Acronyms"
categories: [wireless]
date: 2023-03-15
summary: A list of acronyms used on this website.
published: true
---

Throughout this website, a tooltip will appear with an acronym's definition when hovering over an acronym.

For example, hover your mouse over the following: SNR. Its definition, "signal-to-noise ratio", should appear.

### Glossary

{% for acronym in site.data.acronyms %}
{{ acronym.short }}: {{ acronym.long }}  
{% endfor %}

{% for acronym in site.data.acronyms %}
  *[{{ acronym.short }}]: {{ acronym.long }}  
{% endfor %}
