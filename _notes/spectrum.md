---
layout: note
title: "6G Frequency Spectrum: Sub-6 GHz, Upper Mid-Band, mmWave, and Sub-THz"
categories: [wireless,mmwave]
date: 2023-03-15
summary: An introduction to the different frequency bands proposed for use in 6G cellular systems.
published: true
---

{% assign f = "/fig/spectrum.svg" %}
{% assign c = "The different frequency bands used in modern cellular systems." %}
{% include image.html file=f caption=c link=false %}

Here are a few good papers to read more on this topic:
- Paper 1
- Paper 2
- Paper 3
