---
layout: note
title: "Beamforming Architectures: Digital, Analog, and Hybrid"
categories: [wireless,mmwave]
date: 2023-03-15
summary: An introduction to different beamforming architectures.
summary-extended: >
   Test
published: true
---

Before reading this note, it is recommended to first read the following note:
- [Fundamentals of Antenna Arrays](/notes/arrays)

### Digital Beamforming

In traditional, lower-frequency multi-antenna communication systems, it has been common to use fully-digital beamforming. 
This requires a dedicated RF chain and digital-to-analog converter (DAC) at each transmit antenna and a dedicated RF chain and analog-to-digital converter (ADC) at each receive antenna. 
Scaling this to the dozens or hundreds of antennas in high-frequency transceivers---such as those at millimeter wave (mmWave) frequencies---is typically not practically viable. 

{% assign f = "/fig/hybrid.svg" %}
{% assign c = "Digital beamforming." %}
{% include image.html file=f caption=c link=false %}

### Analog Beamforming

{% assign f = "/fig/hybrid.svg" %}
{% assign c = "Analog beamforming." %}
{% include image.html file=f caption=c link=false %}

### Hybrid Digital/Analog Beamforming


Fully-digital beamforming does not scale well to wireless systems employing many antennas, such as those operating at millimeter wave frequencies. Instead, hybrid beamforming architectures have been proposed which split beamforming into two stages: a digital stage and an analog stage. This allows 

Instead, a so-called hybrid beamforming architecture has often been adopted for mmWave transceivers, as illustrated in Figure 2.3. Through the combination of analog and digital beamforming, mmWave transcievers with hybrid beamforming can deliver high beamforming gain while also multiplexing multiple multiple-input multiple-output (MIMO) spatial streams---all with a reduced number of RF chains.
Digital beamforming takes place at baseband in software or digital logic, whereas analog beamforming is implemented at RF using a network of phase shifters, which are digitally controlled in practice. In addition to phase control, some analog beamforming networks also offer amplitude control through digitally-controlled attenuators or variable gain amplifiers (VGAs). Given that phase and amplitude control are digitally controlled with finite resolution, there is a discrete set of analog beamformers which can be realized physically, and the non-convexity of this discrete set has proven to complicate beamforming optimization in mmWave literature.

{% assign f = "/fig/hybrid.svg" %}
{% assign c = "Hybrid digital/analog beamforming architecture." %}
{% include image.html file=f caption=c link=false %}

Here are a few good papers to read more on hybrid beamforming:
- Paper 1
- Paper 2
- Paper 3
