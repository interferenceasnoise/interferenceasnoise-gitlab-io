---
layout: page
title: Research
categories: research
permalink: research/
public: true
hide_in_nav: false
---

<a class="callout-button" href="/publications">Publications &#8674;</a>
&nbsp;&nbsp;<a href="https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works" target="_blank" class="callout-button">Google Scholar &#8599;</a>

<!---<a href="/publications" class="badge badge-blue-inverted">Publications</a>&nbsp;&nbsp;<a href="https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works" target="_blank" class="badge badge-blue-inverted">Google Scholar</a>--->

### Overview

In the {{ site.lab-name }} at UCLA, we use mathematical tools, machine learning, and real-world hardware to analyze, model, optimize, and prototype wireless systems and solutions.
Currently, we are particularly interested in conducting research and creating innovative technologies to drive the design, development, and deployment of future 6G cellular networks.

{% assign f = "/fig/evolution_18.svg" %}
{% assign c = "We aim to develop technologies that will be key in delivering 6G cellular networks by 2030." %}
{% include image.html file=f caption=c link=false width=80 %}

There are a variety of noteworthy challenges and ripe opportunities unfolding as 5G cellular systems evolve and as future 6G systems are imagined.
Today's 5G cellular systems make use of high carrier frequencies, wide bandwidths, and many antennas to meet the high-rate, low-latency demands of modern applications.
It is expected that 6G systems will incorporate new, transformative technologies while employing many of the successful key technologies from 5G.
For years to come, the {{ site.lab-name }} aims to continue to create the technologies that enable future evolutions of wireless communication systems beyond 5G and 6G.

{% assign f = "/fig/evolution_16.svg" %}
{% assign c = "6G will strengthen the foundation of 5G and introduce novel technologies that transform wireless connectivity." %}
{% include image.html file=f caption=c link=false width=90 %}

### Evolution of Communication Systems

Essentially for their entirety, wireless communication systems have undergone constant evolution. 
The evolution of today's wireless communication systems, such as Wi-Fi and cellular systems, is driven largely by applications.
Applications like real-time video calling, video gaming, and machine communcation require low-latency and high-rate wireless connectivity.
To deliver this connectivity, wireless systems like 5G have at their disposal multiple bands of diverse frequency spectrum.
5G cellular systems employ low-frequency spectrum below 6 GHz as well as high-frequency spectrum at millimeter wave (mmWave) frequencies (roughly 30 GHz to 100 GHz).
In addition to sub-6 GHz and mmWave, upper mid-band and sub-THz spectrum are candidate bands being considered for future 6G cellular systems.

{% assign f = "/fig/spectrum_05.svg" %}
{% assign c = "Modern wireless communication systems are making use of both low- and high-frequency spectrum." %}
{% include image.html file=f width=100 caption=c link=false %}

With 5G, wireless topologies have become more heterogeneous, and this is expected to continue with 6G.
There are new ways to deliver wireless connectivity by making use of wireless backhauling and low-Earth orbit satellites, for instance.
Meanwhile, recent advances in AI/ML and antenna arrays have made it possible to create more intelligent and powerful wireless techniques than ever before.
Together, new applications, new spectrum, new topologies, and new tools will continue to evolve wireless connectivity to enable emerging and unforeseen applications.

{% assign f = "/fig/evolution_13.svg" %}
{% assign c = "New spectrum, topologies, and tools will deliver the connectivity required by emerging applications." %}
{% include image.html file=f caption=c link=false width=80 %}


### Our Approach to Wireless Research: Bridging Theory and Practice

{% include text-image-begin.html image="fig/phased_arrays_04.jpg" width=45 mobile=1 float='right' %}
<div markdown="1">
Our research spans from purely theoretical to heavily experimental.
For example, some of our work involves crafting novel designs and solutions based on mathematical modeling of wireless systems. 
These designs and solutions may be created using machine learning or more classical optimization techniques.
Often times, we incorporate practical constraints and considerations---such as hardware limitations---into our designs and solutions to create solutions that better translate to the real world.

We use extensive simulation built on established models and techniques to evaluate our proposed designs and solutions.
Where possible and practical, however, we also incorporate experimental components into our research efforts.
To do this, we use real-world hardware (like mmWave phased arrays) to test and evaluate proposed solutions. 
We also use real-world hardware to take measurements to characterize wireless systems and to explore the viability of future technologies.

We find that incorporating experimental components to wireless research has a few important benefits, as outlined and illustrated below.
- Experimentation can be used to validate proposed designs with real-world hardware in real-world settings.
- It allows us to revise designs to incorporate real-world limitations and nonidealities.
- It allows us to formulate models and create simulations that better reflect the real world.
- It fosters new research problems, new theory, and new hardware requirements.

{% assign f = "/fig/experimentation_04.svg" %}
{% assign c = "Feedback from experimentation often yields new techniques and new problems." %}
{% include image.html file=f width=90 caption=c link=false %}

Listed below are some of our papers that are moreso on the purely theoretical side:
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range](/pdf/pub/bflrdr.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf) <span class="badge badge-purple-inverted">Theory</span> 

The papers below, on the other hand, incorporate experimental components:
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
</div>
{% include text-image-end.html %}


<!---
### Our Approach to Wireless Research: Bridging Theory and Practice

Our research spans from purely theoretical to heavily experimental.
For example, some of our work involves crafting novel designs and solutions based on mathematical modeling of wireless systems. 
These designs and solutions may be created using machine learning or more classical optimization techniques.
Often times, we incorporate practical constraints and considerations---such as hardware limitations---into our designs and solutions to create solutions that better translate to the real world.

We use extensive simulation using established models and techniques to evaluate our proposed designs and solutions.
Where possible and practical, however, we also incorporate experimental components into our research efforts.
To do this, we often use real-world hardware to test and evaluate proposed solutions. 
We also use real-world hardware to take measurements to characterize wireless systems and to explore the viability of future technologies.

{% assign f = "/fig/phased_arrays_04.jpg" %}
{% assign c = "Experimental research in collaboration with Prof. Ahmed Alkhateeb at Arizona State University." %}
{% include image.html file=f width=75 caption=c link=false %}

We find that incorporating experimental components to wireless research has a few important benefits, as outlined and illustrated below.
- It allows us to validate proposed designs with real-world hardware in real-world settings.
- It allows us to revise designs to incorporate real-world limitations and nonidealities.
- It allows us to formulate models and create simulations that better reflect the real world.
- It fosters new research problems, new theory, and new hardware requirements.

{% assign f = "/fig/experimentation_03.svg" %}
{% assign c = "The feedback from experimentation can be extremely fruitful in the research process." %}
{% include image.html file=f width=90 caption=c link=false %}

Listed below are some of our papers that are more on the purely theoretical side:
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf)
- [Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range](/pdf/pub/bflrdr.pdf)
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf)

The papers below, on the other hand, incorporate experimental components:
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf)
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf)
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf)
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf)
--->

### Our Current Research

We conduct research on a variety of topics in wireless, all of which have relevant applications in next-generation wireless communication systems, like 6G cellular networks.
Our current research projects are on the topics of:
- **Full-Duplex Wireless Systems** --- to increase throughput, reduce latency, and broaden coverage; 
- **Satellite Communication Systems** --- to deliver near-global wireless connectivity;
- **Intelligent Beam Alignment** --- to efficiently deploy high-frequency communication systems;
- **Joint Communication and Sensing** --- to enable emerging and unforeseen applications.

#### Full-Duplex Wireless Communication Systems

<!---{% include text-image-begin.html image="/fig/fdx_users_type_08.svg" width=35 mobile=1 float='right' %}--->
<div markdown="1">
Since their inception more than a century ago, wireless systems have almost exclusively not been able to transmit and receive at the same time using the same frequency spectrum.
They have historically operated in a half-duplex fashion, separating transmit and receive signals in either the time domain or the frequency domain.
Our research aims to change that. 

{% assign f1 = "/fig/resource_tf_06_type_15.svg" %}
{% assign f2 = "/fig/resource_tf_06_type_16.svg" %}
{% assign c = 'Full-duplex makes more efficient use of radio resources than traditional half-duplex techniques by allowing both downlink and uplink to be served at the same time over the same frequency spectrum.' %}
{% include image-double.html file1=f1 file2=f2 width1=48 width2=48 caption=c link=false %}

We develop novel techniques allowing a device to simultaneously transmit and receive using the same spectrum---called _in-band full-duplex_ operation.
We use mathematical modeling and optimization tools to create solutions that can enable full-duplex by cancelling what is called self-interference: the interference a transceiver inflicts on itself when simultaneously transmitting and receiving at the same frequency.
We implement our designs on actual hardware to demonstrate their effectiveness in real-world systems.

{% assign f = "/fig/applications_multipanel_type_66.svg" %}
{% assign c = "A full-duplex base station transmits to one user while receiving from another user over the same spectrum." %}
{% include image.html file=f width=60 caption=c link=false %}

With full-duplex capability unlocked, a wireless communication system can deliver higher throughput, lower latency, and broader coverage by making more efficient use of frequency spectrum.
Here are a few suggested papers of ours that you can read on this topic:
- [Full-Duplex Transceivers for Next-Generation Wireless Communication Systems](/pdf/pub/chapter.pdf) <span class="badge badge-grey-inverted">Overview</span> 
- [Millimeter-Wave Full Duplex Radios: New Challenges and Techniques](/pdf/pub/wcm.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems](/pdf/pub/lonestar.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread](/pdf/pub/bfsi.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference](/pdf/pub/simodel.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/steer.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
- [Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems](/pdf/pub/realworld.pdf) <span class="badge badge-magenta-inverted">Experimental</span> 
</div>
<!---{% include text-image-end.html %}--->





#### Satellite Communication Systems

{% include text-image-begin.html image="/fig/leo.svg" width=40 mobile=1 float='right' %}
<div markdown="1">
Satellite communication systems are undergoing a renaissance.
Ongoing and upcoming mass satellite deployments by SpaceX (Starlink), OneWeb, and Amazon (Kuiper) are ushering in a new era of connectivity.
These efforts are slated to deploy constellations of thousands of low-Earth orbit (LEO) satellites to deliver of near-global connectivity.

This new topological paradigm introduces technical and regulatory challenges.
For instance, the frequency spectrum on which these LEO satellite systems operate is not strictly exclusive.
This means that spectrum must be shared across systems (i.e., between SpaceX, OneWeb, and Amazon) to some degree.
It is uncertain precisely how these systems can---or should---coexist using the same spectrum most effectively and most ethically.

We explore this problem using optimization and machine learning to better understand how LEO satellite communication systems can strategically coexist.
In this pursuit, we rely on extensive simulation to motivate and validate our work.
Here are two suggested papers on this topic:
- [Downlink Analysis and Evaluation of Multi-Beam LEO Satellite Communication in Shadowed Rician Channels](/pdf/pub/leosr.pdf) <span class="badge badge-purple-inverted">Theory</span> 
- [Downlink Analysis of LEO Multi-Beam Satellite Communication in Shadowed Rician Channels](/pdf/pub/downlinkleo.pdf) <span class="badge badge-purple-inverted">Theory</span> 
</div>
{% include text-image-end.html %}


<!---
{% assign f = "/fig/leo.svg" %}
{% assign c = "Two LEO satellites operate simultaneously using the same spectrum, inflicting interference on one another." %}
{% include image.html file=f width=60 caption=c link=false %}


For more on this topic, below are a couple suggested papers of ours for those interested:
- [Downlink Analysis and Evaluation of Multi-Beam LEO Satellite Communication in Shadowed Rician Channels](/pdf/pub/leosr.pdf)
- [Downlink Analysis of LEO Multi-Beam Satellite Communication in Shadowed Rician Channels](/pdf/pub/downlinkleo.pdf)
-->

#### Beam Alignment for mmWave and Sub-THz Systems

{% include text-image-begin.html image="/fig/alignment_02.svg" width=32 mobile=1 float='right' %}
<div markdown="1">
Closing the link---or establishing a strong connection---between devices is a key challenge in wireless communication systems operating at mmWave (roughly 30 GHz to 100 GHz) and sub-THz (roughly 100 GHz to 1 THz) carrier frequencies.
This stems from the simple fact that path loss generally increases with increased carrier frequencies.
Antenna arrays containing dozens or hundreds of individual antenna elements are used in high-frequency wireless systems to focus energy in a particular direction through what is called _beamforming_.
By focusing energy in this way, received signal power is increased to offset the effects of increased path loss.

Effectively steering a highly directional beam is not a trivial task, however, because it must be steered very accurately---a problem that worsens when devices are mobile.
Slight misalignment of beams can lead to dramatic degradations in signal quality, and re-aligning beams can consume valuable radio resources if too frequent.
This is known as the _beam alignment_ or _beam management_ problem.
To tackle this problem effectively, we use mathematical tools, array signal processing, machine learning, and real-world hardware to create efficient, reliable, and intelligent beam alignment schemes.
</div>
{% include text-image-end.html %}


<!--
{% assign f = "/fig/alignment_02.svg" %}
{% assign c = "Beam alignment is used to close the link between a mmWave base station and user." %}
{% include image.html file=f width=45 caption=c link=false %}
-->

#### Joint Communication and Sensing

{% include text-image-begin.html image="/fig/jcas_01.svg" width=40 mobile=1 float='right' %}
<div markdown="1">
Many applications of today---and those anticipated for tomorrow---require sensing to digitize the real world, interpret human input, or navigate autonomously.
Meanwhile, modern wireless communication systems operate using higher carrier frequencies, wider bandwidths, and more antennas than previous generations of wireless.
And often times, the applications that rely on sensing also rely on wireless connectivity.

This has introduced and motivated the potential for a single system to perform both tasks: wireless communication and wireless sensing.
The many antennas used for communication can also offer fine angular resolution when used for sensing.
Likewise, the wide bandwidths used for communication can also offer fine time (range) resolution when used for sensing.

Capitalizing on this concept of _joint communication and sensing_, our goal is to invent novel signal processing techniques---some using machine learning---to both communicate and sense using the same transceiver and even the same wireless signals.
To do this, we employ our full-duplex expertise to enable high-fidelity sensing by mitigating the effects of self-interference.
In addition, we also investigate how sensing can enhance communication system performance by leveraging knowledge of one's environment.
By implementing proposed approaches using off-the-shelf phased arrays and software-defined radios, we can better validate their effectiveness.
In turn, this also offers us new insights and inspiration and supplies us with real-world data that can enable machine learning and other data-driven techniques.
</div>
{% include text-image-end.html %}

<!--
Many applications of today---and those anticipated for tomorrow---require sensing to digitize the real world, interpret human input, or navigate autonomously.
Meanwhile, modern wireless communication systems operate using higher carrier frequencies, wider bandwidths, and more antennas than previous generations of wireless.
And often times, the applications that rely on sensing also rely on wireless connectivity.

This has introduced and motivated the potential for a single system to perform both tasks: wireless communication and wireless sensing.
The many antennas used for communication can also offer fine angular resolution when used for sensing.
Likewise, the wide bandwidths used for communication can also offer fine time (range) resolution when used for sensing.

Capitalizing on this concept of _joint communication and sensing_, our goal is to invent novel signal processing techniques---some using machine learning---to both communicate and sense using the same transceiver and even the same wireless signals.
To do this, we employ our full-duplex expertise to enable high-fidelity sensing by mitigating the effects of self-interference.
In addition, we also investigate how sensing can enhance communication system performance by leveraging knowledge of one's environment.
By implementing proposed approaches using off-the-shelf phased arrays and software-defined radios, we can better validate their effectiveness.
In turn, this also offers us new insights and inspiration and supplies us with real-world data that can enable machine learning and other data-driven techniques.

{% assign f = "/fig/jcas_01.svg" %}
{% assign c = "Modern applications rely on wireless sensing and wireless communication." %}
{% include image.html file=f width=50 caption=c link=false %}
-->


