---
layout: page
title: Group
categories: group
permalink: group/
published: true
---

{% assign ppl = (site.data.members | sort: 'date') | reverse %}
{% assign ppl = site.data.members %}


{% assign f = "/people/group_f24.jpg" %}
{% assign c = "December 2024 in Westwood. Front L/R: Joohyun, Chaeyeon. Back L/R: Ethan, Caden, Hyunwoo, Sungho, Ian, Xinsong, Samuel, Pranay, Jeong Min." %}
{% include image.html file=f caption=c width=100 link=true %}

<br>

### Prof. Ian Roberts

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'prof' %}
{% if p.graduated == false %}
{% include student-current.html %}

{% endif %}
{% endif %}
{% endif %}
{% endfor %}

<br>


### Current Students

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'grad' %}
{% if p.graduated == false %}
{% include student-current.html %}  
<br>
{% endif %}
{% endif %}
{% endif %}
{% endfor %}

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'ugrad' %}
{% if p.graduated == false %}
{% include student-current.html %}  
<br>
{% endif %}
{% endif %}
{% endif %}
{% endfor %}

<br>

### Current Visiting Professor

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'visitor-prof' %}
{% if p.graduated == false %}
{% include student-current.html %}

{% endif %}
{% endif %}
{% endif %}
{% endfor %}

<br>

### Current Visiting Researchers

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'visitor' %}
{% if p.graduated == false %}
{% include student-current.html %}  
<br>
{% endif %}
{% endif %}
{% endif %}
{% endfor %}

<br>


<!---
### Current Interns

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'intern' %}
{% if p.graduated == false %}
{% include student-current.html %}  
<br>
{% endif %}
{% endif %}
{% endif %}
{% endfor %}

<br>

--->

### Alumni

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'grad' %}
{% if p.graduated == true %}
{% include student-alumni.html %}
{% endif %}
{% endif %}
{% endif %}
{% endfor %}

{% for p in ppl %}
{% if p.display == true %}
{% if p.type == 'ugrad' %}
{% if p.graduated == true %}
{% include student-alumni.html %}
{% endif %}
{% endif %}
{% endif %}
{% endfor %}


<!---<br>--->

<!---
### Old Group Photos

{% assign f = "/group/group_01.jpg" %}
{% assign c = "Group in January 2022." %}
{% include image.html file=f caption=c link=false %}

<br>

{% assign f = "/group/group_01.jpg" %}
{% assign c = "Group in January 2021." %}
{% include image.html file=f caption=c link=false %}

-->
