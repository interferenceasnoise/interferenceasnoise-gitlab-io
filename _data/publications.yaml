- title: Beamforming Cancellation Design for Millimeter-Wave Full-Duplex
  authors: "I. P. Roberts and S. Vishwanath"
  journal: IEEE Global Communications Conference
  venue: IEEE Global Communications Conference
  location: Waikoloa, Hawaii, USA
  date: 2019-12-09
  arxiv: https://arxiv.org/abs/1908.06505
  ieee: https://ieeexplore.ieee.org/document/9013116
  pdf: /pdf/pub/bfc.pdf
  slides: /pdf/pub/bfc_slides.pdf
  short: Enabling in-band full-duplex at mmWave via strategic MIMO precoding and combining.
  type: conference

- title: Frequency-Selective Beamforming Cancellation Design for Millimeter-Wave Full-Duplex
  authors: "I. P. Roberts, H. B. Jain, and S. Vishwanath"
  journal: IEEE International Conference on Communications
  venue: IEEE International Conference on Communications
  location: <del>Dublin, Ireland</del> Virtual
  date: 2020-06-06
  arxiv: https://arxiv.org/abs/1910.11983
  ieee: https://ieeexplore.ieee.org/document/9149419
  pdf: /pdf/pub/fsbfc.pdf
  slides: /pdf/pub/fsbfc_slides.pdf
  youtube: https://www.youtube.com/watch?v=fwCXDR9Yk70
  short: Extending our beamforming cancellation design for mmWave full-duplex to frequency-selective settings.
  type: conference
  
- title: Enabling In-Band Coexistence of Millimeter-Wave Communication and Radar
  authors: "H. B. Jain, I. P. Roberts, and S. Vishwanath"
  journal: IEEE International Radar Conference
  venue: IEEE International Radar Conference
  location: <del>Washington, D.C., USA</del> Virtual
  date: 2020-04-28
  arxiv: https://arxiv.org/abs/1911.11283
  ieee: https://ieeexplore.ieee.org/document/9114802
  pdf: /pdf/pub/radar2020.pdf
  slides: /pdf/pub/radar2020_slides.pdf
  youtube: https://www.youtube.com/watch?v=JNRvAfQ9zs0
  short: A look at how concepts of mmWave full-duplex can enable in-band coexistence of communication and radar.
  type: conference
  
- title: Equipping Millimeter-Wave Full-Duplex with Analog Self-Interference Cancellation
  authors: "I. P. Roberts, H. B. Jain, and S. Vishwanath"
  journal: IEEE International Conference on Communications Workshops
  venue: IEEE International Conference on Communications, Workshop on Full-Duplex Communications for Future Wireless Networks
  location: <del>Dublin, Ireland</del> Virtual
  date: 2020-06-07
  arxiv: https://arxiv.org/abs/2002.02127
  ieee: https://ieeexplore.ieee.org/document/9145231
  pdf: /pdf/pub/asic.pdf
  slides: /pdf/pub/asic_slides.pdf
  youtube: https://www.youtube.com/watch?v=DSKUMjyja-Y
  short: Using analog self-interference cancellation to supplement beamforming to enable mmWave full-duplex.
  type: conference
  
- title: Collision Detection in Dense Wi-Fi Networks using Self-Interference Cancellation
  authors: "R. K. Mishra, Y. Chen, I. P. Roberts, and S. Vishwanath"
  journal: IEEE International Conference on Communications Workshops
  venue: IEEE International Conference on Communications, Workshop on Full-Duplex Communications for Future Wireless Networks
  location: <del>Dublin, Ireland</del> Virtual
  date: 2020-06-08
  ieee: https://ieeexplore.ieee.org/document/9145114
  pdf: /pdf/pub/collision.pdf
  short: Improving dense network performance by leveraging self-interference cancellation to detect and avoid collisions.
  type: conference
  
- title: "Millimeter-Wave Full Duplex Radios: New Challenges and Techniques"
  authors: "I. P. Roberts, J. G. Andrews, H. B. Jain, and S. Vishwanath"
  journal: "IEEE Wireless Communications Magazine"
  date: 2021-02-01
  pdf: /pdf/pub/wcm.pdf
  ieee: https://ieeexplore.ieee.org/document/9363024
  arxiv: https://arxiv.org/abs/2009.06048
  short: Outlining key system-level challenges, unknowns, and potential solutions for mmWave full-duplex.
  type: journal
  bibtex: /pdf/pub/wcm.txt

- title: "Hybrid Beamforming for Millimeter Wave Full-Duplex under Limited Receive Dynamic Range"
  authors: "I. P. Roberts, J. G. Andrews, and S. Vishwanath"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2021-06-15
  pdf: /pdf/pub/bflrdr.pdf
  ieee: https://ieeexplore.ieee.org/document/9456023
  arxiv: https://arxiv.org/abs/2012.11647
  short: Beamforming design for mmWave full-duplex preventing self-interference from saturating the receive chain.
  type: journal
  bibtex: /pdf/pub/bflrdr.txt
  
- title: "Millimeter Wave Analog Beamforming Codebooks Robust to Self-Interference"
  authors: "I. P. Roberts, H. B. Jain, S. Vishwanath, and J. G. Andrews"
  journal: IEEE Global Communications Conference
  date: 2021-12-02
  pdf: /pdf/pub/robustcb.pdf
  arxiv: https://arxiv.org/abs/2105.13450
  ieee: https://ieeexplore.ieee.org/document/9686032
  short: A design methodology for codebooks that offer high beamforming gain while minimizing self-interference.
  youtube: https://youtu.be/VQTctSlYl4g
  type: conference
  
- title: "Downlink Analysis of LEO Multi-Beam Satellite Communication in Shadowed Rician Channels"
  authors: "E. Kim, I. P. Roberts, P. A. Iannucci, and J. G. Andrews"
  journal: IEEE Global Communications Conference
  date: 2021-12-01
  pdf: /pdf/pub/downlinkleo.pdf
  arxiv: https://arxiv.org/abs/2107.13776
  ieee: https://ieeexplore.ieee.org/document/9685293
  short: Characterizing downlink signal and interference in LEO satellite systems under Shadowed Rician channels.
  type: conference
  
- title: "MIMO for MATLAB: A Toolbox for Simulating MIMO Communication Systems"
  authors: "I. P. Roberts"
  date: 2021-11-01
  journal: arXiv
  pdf: /pdf/pub/mfm.pdf
  arxiv: https://arxiv.org/abs/2111.05273
  short: A MATLAB toolbox for simulating MIMO communication systems at the symbol-level.
  type: conference
  
- title: "System-Level Analysis of Full-Duplex Self-Backhauled Millimeter Wave Networks"
  authors: "M. Gupta, I. P. Roberts, and J. G. Andrews"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2022-09-01
  pdf: /pdf/pub/fdiab.pdf
  arxiv: https://arxiv.org/abs/2112.05263
  ieee: https://ieeexplore.ieee.org/document/9905974
  short: Highlighting the rate and latency improvements introduced by full-duplex in IAB networks.
  type: journal
  bibtex: /pdf/pub/fdiab.txt
  
- title: "28 GHz Phased Array-Based Self-Interference Measurements for Millimeter Wave Full-Duplex"
  authors: "A. Chopra, I. P. Roberts, T. Novlan, and J. G. Andrews"
  journal: "IEEE Wireless Communications and Networking Conference"
  date: 2022-04-12
  short: Spatially dense measurements of self-interference in full-duplex mmWave systems.
  type: conference
  pdf: /pdf/pub/simeasure.pdf
  arxiv: https://arxiv.org/abs/2203.02809
  youtube: https://youtu.be/aqFdd7M1mi0
  slides: /pdf/pub/simeasure_slides.pdf
  ieee: https://ieeexplore.ieee.org/document/9771589
  category: "Experimental" 
  
- title: "Beamformed Self-Interference Measurements at 28 GHz: Spatial Insights and Angular Spread"
  authors: "I. P. Roberts, A. Chopra, T. Novlan, S. Vishwanath, and J. G. Andrews"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2022-06-14
  short: The first extensive measurement campaign of self-interference in mmWave systems using phased arrays.
  type: journal
  ieee: https://ieeexplore.ieee.org/document/9794906
  pdf: /pdf/pub/bfsi.pdf
  code: /bfsi
  bibtex: /pdf/pub/bfsi.txt
  category: "Experimental" 
  
- title: "STEER: Beam Selection for Full-Duplex Millimeter Wave Communication Systems"
  authors: "I. P. Roberts, A. Chopra, T. Novlan, S. Vishwanath, and J. G. Andrews"
  journal: "IEEE Transactions on Communications"
  date: 2022-08-16
  short_: A beamforming-based solution enabling full-duplex mmWave systems while accommodating beam alignment and limited channel knowledge. 
  short: Slightly shifting beams to significantly reduce self-interference and enable mmWave full-duplex.
  type: journal
  pdf: /pdf/pub/steer.pdf
  arxiv: https://arxiv.org/abs/2207.07281
  ieee: https://ieeexplore.ieee.org/document/9856612
  poster: /pdf/pub/steer_poster.pdf
  bibtex: /pdf/pub/steer.txt
  code: /steer
  category: "Experimental" 
  
- title: "LoneSTAR: Analog Beamforming Codebooks for Full-Duplex Millimeter Wave Systems"
  authors: "I. P. Roberts, S. Vishwanath, and J. G. Andrews"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2023-09-01
  short_: Enabling full-duplex mmWave systems through the design of beamforming codebooks that mitigate self-interference and deliver high gain.
  short: Beamforming codebooks designed to be robust to self-interference, specifically for full-duplex mmWave systems.
  type: journal
  pdf: /pdf/pub/lonestar.pdf
  arxiv: https://arxiv.org/abs/2206.11418
  ieee: https://ieeexplore.ieee.org/document/10022041
  bibtex: /pdf/pub/lonestar.txt
  code: /lonestar
  
- title: "Downlink Analysis and Evaluation of Multi-Beam LEO Satellite Communication in Shadowed Rician Channels"
  authors: "E. Kim, I. P. Roberts, and J. G. Andrews"
  journal: "IEEE Transactions on Vehicular Technology"
  date: 2024-02-01
  short: Analysis and simulation of downlink performance metrics in multi-beam LEO satellite communication systems.
  type: journal
  pdf: /pdf/pub/leosr.pdf
  arxiv: https://arxiv.org/abs/2207.06663
  bibtex: /pdf/pub/leosr.txt
  ieee: https://ieeexplore.ieee.org/document/10243147
  
- title: "Full-Duplex Transceivers for Next-Generation Wireless Communication Systems"
  authors: "I. P. Roberts and H. A. Suraweera"
  journal: "Fundamentals of 6G Communications and Networking published by Springer"
  date: 2024-01-01
  short: A book chapter overviewing full-duplex technology and its role in next-generation wireless networks.
  type: book
  pdf: /pdf/pub/chapter.pdf
  arxiv: http://arxiv.org/abs/2210.08094
  bibtex: /pdf/pub/chapter.txt
  publisher: "https://link.springer.com/chapter/10.1007/978-3-031-37920-8_16"
  
- title: "Spatial and Statistical Modeling of Multi-Panel Millimeter Wave Self-Interference"
  authors: "I. P. Roberts, A. Chopra, T. Novlan, S. Vishwanath, and J. G. Andrews"
  journal: "IEEE Journal on Selected Areas in Communications"
  date: 2023-09-02
  short: The first measurement-backed spatial and statistical model of mmWave self-interference.
  type: journal
  pdf: /pdf/pub/simodel.pdf
  arxiv: http://arxiv.org/abs/2210.08093
  bibtex: /pdf/pub/simodel.txt
  code: /simodel
  ieee: https://ieeexplore.ieee.org/document/10176267
  category: "Experimental" 
  
- title: "Real-World Evaluation of Full-Duplex Millimeter Wave Communication Systems"
  authors: "I. P. Roberts, Y. Zhang, T. Osman, and A. Alkhateeb"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2024-03-01
  short: Measurements and experimental evaluation of full-duplex mmWave systems using 60 GHz phased arrays.
  type: journal
  pdf: /pdf/pub/realworld.pdf
  arxiv: http://arxiv.org/abs/2307.10523
  bibtex: /pdf/pub/realworld.txt
  poster: /pdf/pub/steerplus_poster.pdf
  ieee: https://ieeexplore.ieee.org/document/10477869
  category: "Experimental" 
  
- title: "Feasibility Analysis of In-Band Coexistence in Dense LEO Satellite Communication Systems"
  authors: "E. Kim, I. P. Roberts, and J. G. Andrews"
  journal_: "IEEE Transactions on Vehicular Technology"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2024-12-12
  short: Assessing the feasibility of spectrum sharing between dense LEO satellite systems, such as Starlink and Kuiper.
  type: journal
  pdf: /pdf/pub/coexleo.pdf
  arxiv: https://arxiv.org/abs/2311.18250
  bibtex: /pdf/pub/coexleo.txt
  ieee: https://ieeexplore.ieee.org/document/10797644
  
- title: "Analog Beamforming for In-Band Full-Duplex Phased Arrays with Quantized Phase Shifters under a Per-Antenna Received Power Constraint"
  authors: "A. Liu, I. P. Roberts, T. Riihonen, and W. Sheng"
  journal: "IEEE Signal Processing Letters"
  date: 2024-06-07
  short: An analog beamforming design which cancels self-interference at each receive antenna.
  type: journal
  pdf: /pdf/pub/abfparpc.pdf
  arxiv: https://arxiv.org/abs/2401.13914
  bibtex: /pdf/pub/abfparpc.txt
  ieee: https://ieeexplore.ieee.org/document/10551905
    
- title: "STEER+: Robust Beam Refinement for Full-Duplex Millimeter Wave Communication Systems"
  authors: "I. P. Roberts, Y. Zhang, T. Osman, and A. Alkhateeb"
  journal: "Asilomar Conference on Signals, Systems, and Computers"
  date: 2023-11-01
  short: Refining the beams of a full-duplex mmWave base station to increase SNR and reduce self-interference.
  type: conference
  pdf: /pdf/pub/steerplus.pdf
  arxiv_: https://arxiv.org/abs/2207.07281
  ieee: https://ieeexplore.ieee.org/document/10476919
  poster: /pdf/pub/steerplus_poster.pdf
  bibtex: /pdf/pub/steerplus.txt
  category: "Experimental" 
  
- title: "Nonlinear Self-Interference Cancellation with Learnable Orthonormal Polynomials for Full-Duplex Wireless Systems"
  authors: "H. Lee, J. Kim, G. Choi, I. P. Roberts, J. Choi, and N. Lee"
  journal_: ""
  date: 2024-03-01
  short: Adaptive digital self-interference cancellation for non-stationary transmit signals.
  type: preprint
  pdf: /pdf/pub/aoplms.pdf
  arxiv: https://arxiv.org/abs/2403.11094
  bibtex: /pdf/pub/aoplms.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category: "Experimental" 
  
- title: "A Survey on Advancements in THz Technology for 6G: Systems, Circuits, Antennas, and Experiments"
  authors: "S. Thomas, J. S. Virdi, A. Babakhani, and I. P. Roberts"
  journal_: ""
  date: 2024-06-10
  short: A survey of recent developments in THz technology for 6G communications.
  type: preprint
  pdf: /pdf/pub/thz-survey.pdf
  arxiv: https://arxiv.org/abs/2407.01957
  bibtex_: /pdf/pub/aoplms.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
  
- title: "Power Allocation for Frequency-Modulated OFDM Wireless Systems"
  authors: "J. Son, S. Choi, I. P. Roberts, and D. Hong"
  journal: IEEE Vehicular Technology Conference
  date: 2024-10-07
  pdf: /pdf/pub/fm-ofdm.pdf
  arxiv_: https://arxiv.org/abs/2107.13776
  ieee: https://ieeexplore.ieee.org/document/10757575
  short: A power allocation scheme for wireless systems employing FM-OFDM modulation.
  type: conference
  
- title: "Analog Beamforming Codebooks for Wideband Full-Duplex Millimeter-Wave Systems"
  authors: "S. Cho and I. P. Roberts"
  journal_: ""
  date: 2024-11-06
  short: Analog beamforming codebooks tackling frequency-selective self-interference and beam squint.
  type: preprint
  pdf: /pdf/pub/wideband-fd-codebooks.pdf
  arxiv: http://arxiv.org/abs/2411.03691
  bibtex_: /pdf/pub/aoplms.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
  
- title: "Beam Tracking for Full-Duplex User Terminals in Low Earth Orbit Satellite Communication Systems"
  authors: "C. Kim, J. Son, D. Hong, and I. P. Roberts"
  journal_: ""
  date: 2024-11-05
  short: Selecting a user terminal's beams to follow LEO satellites across their orbits while reducing self-interference.
  type: preprint
  pdf: /pdf/pub/fd-beam-tracking.pdf
  arxiv: http://arxiv.org/abs/2411.03606
  bibtex_: /pdf/pub/aoplms.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
  
- title: "Splitting Messages in the Dark --- Rate-Splitting Multiple Access for FDD Massive MIMO Without CSI Feedback"
  authors: "N. Kim, I. P. Roberts, and J. Park"
  journal: "IEEE Transactions on Wireless Communications"
  date: 2025-01-23
  short: Downlink MU-MIMO in FDD massive MIMO systems using only uplink CSI.
  type: journal
  pdf: /pdf/pub/splitting-messages-fdd.pdf
  arxiv: http://arxiv.org/abs/2405.00979
  bibtex_: /pdf/pub/aoplms.txt
  ieee: https://ieeexplore.ieee.org/document/10851806
  category_: "Experimental" 
  
- title: "Active Beam Learning for Full-Duplex Wireless Systems"
  authors: "J. M. Kong and I. P. Roberts"
  journal_: "Asilomar Conference on Signals, Systems, and Computers"
  date: 2024-12-02
  short: Learning to design beams for full-duplex without explicit channel knowledge.
  type: preprint
  pdf: /pdf/pub/active-beam-learning-fd.pdf
  arxiv: http://arxiv.org/abs/2412.03746
  bibtex: /pdf/pub/active-beam-learning-fd.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
  
- title: "Adaptive Cell Range Expansion in Multi-Band UAV Communication Networks"
  authors: "X. Feng and I. P. Roberts"
  journal_: "ISIT"
  date: 2024-11-30
  short: Analyzing and optimizing UAV communication networks using stochastic geometry.
  type: preprint
  pdf: /pdf/pub/cre-uav.pdf
  arxiv: http://arxiv.org/abs/2411.18123
  bibtex_: /pdf/pub/active-beam-learning-fd.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
  
- title: "Can TDD Be Employed in LEO SatCom Systems? Challenges and Potential Approaches"
  authors: "H. Lee, I. P. Roberts, J. Heo, J. Son, H. Kim, Y. Lee, and D. Hong"
  journal_: ""
  short: "Motivating future investigation into TDD in LEO SatCom systems and solutions to enable such."
  date: 2025-02-11
  type: preprint
  pdf: /pdf/pub/leo-tdd-mag.pdf
  arxiv: https://arxiv.org/abs/2502.08179
  bibtex_: /pdf/pub/active-beam-learning-fd.txt
  ieee_: https://ieeexplore.ieee.org/document/10243147
  category_: "Experimental" 
