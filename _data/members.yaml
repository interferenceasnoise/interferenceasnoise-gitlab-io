- name: Ian Roberts
  photo: "ipr.jpg"
  site: https://ianproberts.com
  scholar: https://scholar.google.com/citations?hl=en&user=RDAx4ycAAAAJ&view_op=list_works
  github_: test
  linkedin_: test
  cv_: test
  interests_: "test"
  experience_: "test"
  type: prof
  display: true
  graduated: false
  bio: >
   is an Assistant Professor in the [Department of Electrical and Computer Engineering](https://ee.ucla.edu) at [UCLA](https://ucla.edu), where he leads the Wireless Lab in conducting theoretical and experimental research on wireless communication and sensing. Prior to joining UCLA, he completed his Ph.D. at the [University of Texas at Austin](https://www.ece.utexas.edu), where he was an NSF Graduate Research Fellow in the [Wireless Networking and Communications Group](https://wncg.org) and the [6G@UT Research Center](https://6g-ut.org/).<br>&emsp;He has industry experience developing and prototyping wireless technologies at AT&T Labs, Amazon, GenXComm (startup), Sandia National Labs, and Dynetics (defense contractor). During his time at [GenXComm](https://gxc.io), he created and demonstrated their earliest wireless solutions combining RF, photonic, and digital signal processing for interference cancellation. In 2023, he received the [Andrea Goldsmith Young Scholars Award](https://comt.committees.comsoc.org/awards/andrea-goldsmith-young-scholars-award/) from the [Communication Theory Technical Committee](https://comt.committees.comsoc.org) of the [IEEE Communications Society](https://www.comsoc.org/) for his work on the theory and practice of full-duplex millimeter-wave communication systems. He is a licensed amateur radio operator; his callsign is KE0QVW.
  
- name: Samuel Li
  photo: samuel.jpg
  start: 2023
  finish: 2028
  degree: Ph.D.
  linkedin: https://www.linkedin.com/in/hanchengli/
  github: https://github.com/hcsamuelli
  ad_: "Please feel free to reach out to me with summer internship opportunities!"
  type: grad
  display: true
  graduated: false
  bio: "is a Ph.D. student at the University of California, Los Angeles. He is currently pursuing his degree in Electrical Engineering. He received his B.S. in Electrical Engineering at UCLA with cum laude in 2022. His research interests include machine learning / signal processing for applications in 5G in-band full-duplex operations."

- name: Jeong Min Kong
  photo: jeongmin.jpg
  start: 2023
  finish: 2028
  degree: Ph.D.
  linkedin_: https://www.linkedin.com/in/hanchengli/
  github_: https://github.com/hcsamuelli
  ad_: "Please feel free to reach out to me with summer internship opportunities!"
  type: grad
  display: true
  graduated: false
  bio: "is a Ph.D. student at the University of California, Los Angeles. He received his B.A.Sc. and M.A.Sc. in Electrical and Computer Engineering from the University of Toronto in 2022 and 2024, respectively. His research interests span wireless communications, information theory, and machine learning, including full-duplex systems, satellite communications, and distributed learning."
  
- name: Neil Kane
  photo: neil.jpg
  start: 2023
  finish: 2028
  degree: M.S.
  type: grad
  display: true
  graduated: true
  bio: "is currently working towards his M.S. and B.S. at the University of California, Los Angeles in the Department of Electrical
and Computer Engineering with a specialization in Signals & Systems. He has previously interned at Aptiv and Astranis in the automotive and satellite industries, respectively. His current research interests lie in wireless communication, signal processing, and machine learning."
  signoff: "graduated from UCLA in 2024 with his bachelor's and master's degrees, where he was recognized as an Electrical and Computer Engineering Departmental Scholar. He then joined the Applied Physics Laboratory at Johns Hopkins University as an engineer."
  
- name: Helena Xu
  photo: helena.jpg
  start: 2023
  finish: 2024
  degree: B.S.
  linkedin: https://www.linkedin.com/in/helena-xu424/
  type: ugrad
  display: true
  graduated: true
  bio: "is a fourth-year undergraduate student at UCLA, majoring in Statistics and Psychology. She has prior research experience in biostatistics, comparing the research attitudes of non-Down Syndrome individuals and caretakers of Down Syndrome individuals, particularly with populations relevant to Alzheimer's Disease. Her research interests include machine learning and the application of statistical tools and programming to various fields, including wireless communication."
  signoff: "graduated from UCLA in 2024 with her bachelor's degree. She then joined Stanford University for graduate school."
  
- name: Caden Davis
  photo: caden.jpg
  start: 2023
  finish: 2025
  degree: B.S.
  linkedin: https://www.linkedin.com/in/caden-davis/
  type: ugrad
  display: true
  graduated: false
  bio: "is an undergraduate student at the University of California, Los Angeles majoring in Electrical Engineering. He has previously done research developing communication systems and channel access methods for underwater acoustic nodes. His current research interests cover topics in signal processing including wireless communication and imaging systems."
  
- name: Felipe Jr Figueroa
  photo: felipe.jpg
  start: 2023
  finish: 2027
  degree: B.S.
  type: ugrad
  display: true
  graduated: true
  bio: "is an undergraduate aerospace engineering major at UCLA, with plans to specialize in astronautics. Interested in the expanding intersection between the space industry and wireless technology, Felipe is currently working on satellite communications research at the Wireless Lab. As an aerospace engineering major, Felipe aims to provide a unique lens from which to view challenges in wireless, especially as he progresses further into his studies.<br>&emsp;Aside from his academic endeavors, Felipe is a distance runner who also enjoys drumming in his free time."
  signoff: "concluded his research internship with the Wireless Lab at UCLA, contributing substantially to our experimental efforts through his numerous 3-D printed models, many of which we continue using on a daily basis today."
  
- name: Joohyun Son
  photo: joohyun.jpg
  start: 2023
  finish: 2026
  degree: B.S.
  type: visitor
  display: true
  graduated: false
  bio: "is a Ph.D. student at Yonsei University, Seoul, South Korea. He is currently with the Wireless Lab at UCLA as a visiting graduate researcher under Prof. Ian Roberts. He received his B.S. degree in Electrical and Electronic Engineering from Yonsei University in 2019. His research interests include non-orthogonal multiple access (NOMA), new waveform techniques, and satellite communication."

- name: Xinsong Feng
  photo: xinsong.jpg
  start: 2023
  finish: 2028
  degree: M.S.
  type: grad
  display: true
  graduated: false
  bio: "is a second-year M.S. student in Electrical and Computer Engineering at UCLA. Before joining UCLA, he earned his Bachelor’s degree from Chongqing University in June 2023. His research interests include wireless communication, particularly next-generation mobile communication systems, optimization, and artificial intelligence, aiming to advance AI-enabled communication network design." 
  
- name: Chaeyeon Kim
  photo: chaeyeon.jpg
  start: 2024
  finish: 2027
  degree: B.S.
  type: ugrad
  display: true
  graduated: false
  bio: "is a third-year undergraduate student majoring in Nano Science and Engineering at Yonsei University, Seoul, South Korea. She is currently at UCLA as an exchange student in Materials Engineering. Previously, she has gained experience in CubeSat EGSE GUI implementation and semiconductor device processing. Her current research interests include satellite communications."
  
- name: Ethan Ge
  photo: ethan.jpg
  start: 2024
  finish: 2027
  degree: B.S.
  type: ugrad
  display: true
  graduated: false
  bio: "is a second-year undergraduate student at the University of California, Los Angeles (UCLA) majoring in Electrical Engineering. He has done prior research working on TensorFlow models and is looking towards a future in signal processing. He began interning in the Wireless Lab at UCLA as part of Electrical and Computer Engineering's honors program _Fast Track to Success_."
  
- name: Pranay Boreddy
  photo: pranay.jpg
  start: 2024
  finish: 2027
  degree: B.S.
  type: ugrad
  display: true
  graduated: false
  bio: "is an undergraduate student at UCLA majoring in Computer Science. Currently, Pranay is working on leveraging computer vision for wireless communication. His research interests include applications of signal processing and machine learning. He began interning in the Wireless Lab at UCLA as part of Electrical and Computer Engineering's honors program _Fast Track to Success_."
    
- name: Sungho Cho
  photo: sungho.jpg
  start: 2023
  finish: 2028
  degree: M.S.
  linkedin_: https://www.linkedin.com/in/hanchengli/
  github_: https://github.com/hcsamuelli
  ad_: "Please feel free to reach out to me with summer internship opportunities!"
  type: grad
  display: true
  graduated: false
  bio: "is currently pursuing the M.S. degree in electrical and computer engineering at the University of California, Los Angeles. He received the B.S. degree in electrical engineering from Korea University in 2024. His research interests include MIMO, beamforming, integrated sensing and communication, and their practical implementations."
  
- name: Hyunwoo Lee
  photo: hyunwoo.jpg
  start: 2023
  finish: 2026
  degree: B.S.
  type: visitor
  display: true
  graduated: false
  bio: "is a Ph.D. student at Yonsei University, Seoul, South Korea. He is currently with the Wireless Lab at UCLA as a visiting graduate researcher under Prof. Ian Roberts. He received his B.S. degree in Electrical and Electronic Engineering from Yonsei University in 2021. His research interests include MIMO, channel estimation, advanced duplexing, and satellite communication."
  
- name: Yunseo Lee
  photo: yunseo.jpg
  start: 2023
  finish: 2026
  degree: B.S.
  type: visitor
  display: true
  graduated: false
  bio: "is a Ph.D. student at Yonsei University, Seoul, South Korea. She is currently with the Wireless Lab at UCLA as a visiting graduate researcher under Prof. Ian Roberts. She received her B.S. degree in Electronic and Electrical Engineering from Ewha Womans University in 2023. Her research interests include MIMO, beamforming, resource allocation, and satellite communication."
  
- name: Namyoon Lee
  photo: "namyoon.jpg"
  site: https://wireless-x.korea.ac.kr/professor/
  scholar: https://scholar.google.com/citations?user=gTPowyYAAAAJ
  type: visitor-prof
  display: true
  graduated: false
  bio: >
   is a Professor at the School of Electrical Engineering, Korea University. He received a Ph.D. from The University of Texas at Austin in 2014. He was with the Communications and Network Research Group, Samsung Advanced Institute of Technology, South Korea, from 2008 to 2011, with Nokia Research Center, Berkeley, CA, USA, from 2014 to 2015, and with the Wireless Communications Research, Intel Labs, Santa Clara, CA, USA, from 2015 to 2016. He was an Assistant/Associate Professor at POSTECH from 2016 to 2022. His main research interest is inventing future communication and machine learning systems. Prof. Lee was a recipient of the 2009 Samsung Best Paper Award, the 2012 IEEE Student Best Paper Award (Seoul Section), the 2015 Intel Labs Recognition Award, the 2016 IEEE ComSoc Asia-Pacific Outstanding Young Researcher Award, the 2020 IEEE Best Young Professional Award (Outstanding Nominee), the 2021 IEEE–IEIE Joint Award for Young Engineer and Scientist, the 2021 Headong Young Researcher Award, and the 2022 Best Patent Award for Samsung-POSTECH Research Collaboration. He was an Associate Editor for IEEE Communication Letters from 2019 to 2021 and for IEEE Transactions on Vehicular Technology from 2020 to 2022. Since 2021, he has been an Associate Editor of IEEE Transactions on Wireless Communications and IEEE Transactions on Communications.
