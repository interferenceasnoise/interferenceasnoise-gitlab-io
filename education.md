---
layout: page
title: Education
categories: education
permalink: education/
published: false
hide_in_nav: false
---

{% include text-image-begin.html image="/people/ipr-ctw.jpg" width=35 mobile=1 float='right' %}
<div markdown="1">
In addition to our research contributions, one of the primary missions of the Wireless Lab at UCLA is to educate and train wireless engineers. 
We are invested in equipping students with the technical background and skills to help them be successful in the classroom and in the workplace.
We accomplish this in multiple ways:
- by teaching courses at UCLA
- by hosting short courses
- through outreach
- through partnerships
</div>
{% include text-image-end.html %}


### Short Course: Wireless Academy at UCLA

The Wireless Academy is a six-week short course hosted by Prof. Ian Roberts with the goal of providing students with a high-level introduction to modern wireless networks, such as 5G cellular systems.
Each week, Prof. Ian Roberts will give a lecture on a different topic in wireless.

This short course aims to provide students with knowledge on the fundamental techniques and technologies employed by modern wireless networks.
The Wireless Academy aims to provide students with answers to the following sorts of questions:
- How does my cell phone send and receive data?
- What is 5G, and how does it work?
- What are the underlying technologies of 5G and Wi-Fi?
- What is an antenna, and how does it work?
- Why does my Wi-Fi router have multiple antennas?
- How will wireless networks change over the next 10 years?
- How can wireless networks leverage machine learning and AI?
- What skills do I need to be a wireless engineer at Qualcomm, Samsung, Apple, NVIDIA, Intel, or AT&T?

The hope is that students who attend the Wirelss Academy will be better prepared for their coursework, graduate school, and internship and job opportunities in the wireless field.

**Who:** 
The Wirelss Academy is targeted to undergraduate students and first- and second-year graduate students, but any Bruins interested in wireless communication systems are welcome to join. 
No RSVP is required. 

**When:** The 2023 Wirelss Academy will be held in the Fall Quarter at UCLA with the following tentative schedule:
- Week 1: Wednesday, October 4, at 6 PM 
- Week 2: Wednesday, October 11, at 6 PM
- Week 3: Wednesday, October 18, at 6 PM
- Week 4: Wednesday, October 25, at 6 PM
- Week 5: Wednesday, November 1, at 6 PM
- Week 6: Wednesday, November 8, at 6 PM
- Week 7: Wednesday, November 15, at 6 PM

Each week's lecture will be approximately 1 hour, and Prof. Ian Roberts will stay for an additional 30 minutes after the lecture to answer any questions attendees may have. 

The lectures are not necessarily sequential, so if a student misses a week's lecture, they are able to attend the subsequent lectures without any issue.

**Where:** All short course lectures will be held in room X of the Engineering IV building on the UCLA campus. Unfortunately, no Zoom option will be available at this time. 

This short course is not for credit. No homework, assignments, projects, etc. will be assigned. 

Please email [Prof. Ian Roberts](https://samueli.ucla.edu/people/ian-roberts/) with any questions.

### Courses at UCLA

Past, present and future courses taught by the Wireless Lab: 
- 2024 Winter --- EE XXX Special Topics: Underlying Technologies in Modern Wireless Networks



