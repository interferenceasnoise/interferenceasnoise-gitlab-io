---
layout: page
title: Notes
categories: notes
permalink: notes/
published: false
---

Below are collections of posts, primarily targeted at prospective and current graduate students interested in wireless communication systems.
Most of the technical material is meant to introduce concepts at a high level, not provide in-depth detail.

{% assign notes = site.notes | reverse %}

### Wireless
    {% for note in notes %}
    {% if note.categories contains 'wireless' %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
    {% endif %}
    {% endfor %}


### Collections
    {% for note in notes %}
    {% if note.categories contains 'collection' %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
    {% endif %}
    {% endfor %}


### LaTeX
    {% for note in notes %}
    {% if note.categories contains 'latex' %}
[**{{ note.title }}**]({{ note.url }})  
_{{ note.summary }}_
    {% endif %}
    {% endfor %}


